;; gorilla-repl.fileformat = 1

;; **
;;; # Type and effect system
;; **

;; @@
(use 'nstools.ns)
(ns+ type-effect
  (:like anglican-user.worksheet)
  (:use clojure.set
        lambda-calculus))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; @@
(defn third [l] (first (rest (rest l))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;type-effect/third</span>","value":"#'type-effect/third"}
;; <=

;; @@
;; gamma: map of [x tau] current variable and associated type

;; type of a all but functions: set of pis

;; effect: set of pis which are badly used

;; type of a function:
;; { :code [(a-expr of the function definition) (gamma when function defined)]
;;   (type of input) [(type of output) (effect during application)] }



(defn type-put-in-vector [rho] ;(println (str "put-in-vector " rho))
  (assert (set? rho))
  (set (map #(vector %) rho)))

(defn type-vector [v gamma]
  (apply union (map (comp type-put-in-vector #(gamma % #{})) v)))

(defn type-take-from-vector [rho]
  (assert (set? rho))
  (difference
    (set (map (fn [k] (if (vector? k) (first k) nil)) rho))
    #{nil}))

(defn pi? [k] (keyword? k))

(defn naked-pis [rho]
  (assert (set? rho))
  (set (filter pi? rho)))

(defn until-pi [k]
  (cond
    (pi? k) k
    (vector? k) (until-pi (first k))
    :else nil))
(defn nested-pis [rho]
  (assert set? rho)
  (difference
    (set (map until-pi rho)) 
    #{nil}))

(declare type-effect-e)
(defn type-application [a gamma]
  (let [[f x] a
        tf (gamma f) ; what if f is free?
        _ (if (nil? tf) (println "problem in type-application: function" f "is free"))
        tx (gamma x #{})]
    (if-let [tau-phi (get tf tx)]
      tau-phi
      (let [[f-a f-gamma] (get tf :code)
            f-x (first (second f-a))
            f-e (third f-a)
            f-gamma1 (assoc f-gamma f-x tx)
            [tau phi] (type-effect-e f-e f-gamma1)
            tf1 (assoc tf tx [tau phi])
            gamma1 (assoc gamma f tf1)]
        [tau phi gamma1]))))

(defn type-effect-reduce* [a gamma]
  (let [[_reduce* f init v] a
        tv type-vector v gamma
        ;[tau phi gamma]
        ]
    [#{} #{}])) ;; TODO

(defn type-effect-a [a gamma] ;(println "type-effect-a" a gamma)
  (condp = (a-expr-kind a)
    'variable [(gamma a) #{}]
    'constant [#{} #{}]
    'vector [(type-vector a gamma) #{}]
    'nth (let [rho1 (gamma (second a) #{})
               rho2 (gamma (third a) #{})]
           [(type-take-from-vector rho1)
            (union (naked-pis rho1) (naked-pis rho2))])
    'conj (let [rho1 (gamma (second a) #{})
                rho2 (gamma (third a) #{})]
            [(union rho1 (type-put-in-vector rho2))
             (naked-pis rho1)])
    'reduce* [#{} #{}] ; TODO
    'dirichlet [#{} (naked-pis (gamma (second a) #{}))]
    'discrete [#{} #{}]
    'sample-dirichlet [#{(second a)} (naked-pis (gamma (third a) #{}))]
    'sample [#{} #{}]
    'observe [#{} (nested-pis (gamma (third a) #{}))]
    'predict [#{} (nested-pis (gamma (second a) #{}))]
    'fn [{:code [a gamma]} #{}]
    'application (type-application a gamma)))

(defn type-effect-b [[phi gamma] [x a]]
  (let [[tau phi1 gamma1] (type-effect-a a gamma)
        phi2 (union phi phi1)
        gamma2 (or gamma1 gamma)
        gamma3 (assoc gamma2 x tau)]
    [phi2 gamma3]))

(defn type-effect-bs [bs gamma]
  (reduce type-effect-b [#{} gamma] (partition 2 bs)))

(defn type-effect-e [e gamma]
  (let [[phi gamma1] (type-effect-bs (second e) gamma)]
    ;(println gamma1)
    [(gamma1 (third e)) phi]))

(defn type-effect [program]
  (type-effect-e program {}))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;type-effect/type-effect</span>","value":"#'type-effect/type-effect"}
;; <=

;; **
;;; ---
;;; 
;;; # Examples
;; **

;; @@
;; program after pi-index

(type-effect
  '(let [x19538 (dirichlet alpha) theta0 (sample-dirichlet :pi19701 alpha x19538) x19539 (dirichlet alpha) theta1 (sample-dirichlet :pi19702 alpha x19539) makephi (fn [_19540] (let [x19541 (dirichlet beta-) x19542 (sample-dirichlet :pi19703 beta- x19541)] x19542)) x19546 nil x19543 (makephi x19546) x19547 nil x19544 (makephi x19547) x19548 nil x19545 (makephi x19548) phi [x19543 x19544 x19545] f (fn [psi] (let [x19555 (fn [theta] (let [x19554 (fn [w] (let [x19551 (discrete theta) z (sample x19551) x19553 (nth psi z) x19552 (discrete x19553) _19549 (observe x19552 w)] z))] x19554))] x19555)) x19560 (f phi) x19561 (x19560 theta0) x19556 (x19561 w00) x19562 (f phi) x19563 (x19562 theta0) x19557 (x19563 w01) x19564 (f phi) x19565 (x19564 theta0) x19558 (x19565 w02) x19566 (f phi) x19567 (x19566 theta0) x19559 (x19567 w03) zs0 [x19556 x19557 x19558 x19559] x19572 (f phi) x19573 (x19572 theta1) x19568 (x19573 w10) x19574 (f phi) x19575 (x19574 theta1) x19569 (x19575 w11) x19576 (f phi) x19577 (x19576 theta1) x19570 (x19577 w12) x19578 (f phi) x19579 (x19578 theta1) x19571 (x19579 w13) zs1 [x19568 x19569 x19570 x19571] x19580 [zs0 zs1]] x19580))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-set'>#{</span>","close":"<span class='clj-set'>}</span>","separator":" ","items":[],"value":"#{}"},{"type":"list-like","open":"<span class='clj-set'>#{</span>","close":"<span class='clj-set'>}</span>","separator":" ","items":[],"value":"#{}"}],"value":"[#{} #{}]"}
;; <=

;; @@
;; program-bad-use-beta after pi-index

(type-effect
  '(let [x19584 (dirichlet alpha) theta0 (sample-dirichlet :pi19706 alpha x19584) x19585 (dirichlet alpha) theta1 (sample-dirichlet :pi19707 alpha x19585) makephi (fn [_19586] (let [x19587 (dirichlet beta-) x19588 (sample-dirichlet :pi19708 beta- x19587)] x19588)) x19592 nil x19589 (makephi x19592) x19593 nil x19590 (makephi x19593) x19594 nil x19591 (makephi x19594) phi [x19589 x19590 x19591] f (fn [psi] (let [x19602 (fn [theta] (let [x19601 (fn [w] (let [x19597 (discrete theta) z (sample x19597) x19598 (nth psi z) x19599 1 p (conj x19598 x19599) x19600 (discrete p) _19595 (observe x19600 w)] z))] x19601))] x19602)) x19607 (f phi) x19608 (x19607 theta0) x19603 (x19608 w00) x19609 (f phi) x19610 (x19609 theta0) x19604 (x19610 w01) x19611 (f phi) x19612 (x19611 theta0) x19605 (x19612 w02) x19613 (f phi) x19614 (x19613 theta0) x19606 (x19614 w03) zs0 [x19603 x19604 x19605 x19606] x19619 (f phi) x19620 (x19619 theta1) x19615 (x19620 w10) x19621 (f phi) x19622 (x19621 theta1) x19616 (x19622 w11) x19623 (f phi) x19624 (x19623 theta1) x19617 (x19624 w12) x19625 (f phi) x19626 (x19625 theta1) x19618 (x19626 w13) zs1 [x19615 x19616 x19617 x19618] x19627 [zs0 zs1 phi]] x19627))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-set'>#{</span>","close":"<span class='clj-set'>}</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-keyword'>:pi19708</span>","value":":pi19708"}],"value":"[:pi19708]"}],"value":"[[:pi19708]]"}],"value":"#{[[:pi19708]]}"},{"type":"list-like","open":"<span class='clj-set'>#{</span>","close":"<span class='clj-set'>}</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-keyword'>:pi19708</span>","value":":pi19708"}],"value":"#{:pi19708}"}],"value":"[#{[[:pi19708]]} #{:pi19708}]"}
;; <=

;; @@

;; @@

;; gorilla-repl.fileformat = 1

;; **
;;; #Lambda calculus
;;; 
;;; General properties of the lambda calculus that we have chosen to use to study the collapsing.
;;; 
;; **

;; @@
(use 'nstools.ns)
(ns+ lambda-calculus
  (:like anglican-user.worksheet))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; #### Calculus definitions
;; **

;; @@
(def calculus-keywords 
  {'fn 2
   'let 2
   'nth 2
   'conj 2
   'map 2
   'mapv 2
   'reduce 3
   'dirichlet 1
   'discrete 1
   'sample 1
   'observe 2
   'predict 1
   })
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lambda-calculus/calculus-keywords</span>","value":"#'lambda-calculus/calculus-keywords"}
;; <=

;; @@
(def special-keywords #{'fn 'let})
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lambda-calculus/special-keywords</span>","value":"#'lambda-calculus/special-keywords"}
;; <=

;; @@
(def simple-calculus-keywords 
  (apply dissoc calculus-keywords special-keywords))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lambda-calculus/simple-calculus-keywords</span>","value":"#'lambda-calculus/simple-calculus-keywords"}
;; <=

;; @@
(defn constant? [c] (or (nil? c) (number? c))) 
;; more types of constant may be added later
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lambda-calculus/constant?</span>","value":"#'lambda-calculus/constant?"}
;; <=

;; **
;;; ---
;;; 
;;; #### Predicate
;; **

;; @@
(defn calculus-expr? [expr]
  (or
    (symbol? expr)
    (constant? expr)
    (and (vector? expr)
         (every? calculus-expr? expr))
    (and 
      (seq? expr)
      (or 
        (and (= (simple-calculus-keywords (first expr))
                (count (rest expr)))
             every? calculus-expr? (rest expr))
        (and (= 'let (first expr))
             (vector? (second expr))
             (even? (count (second expr)))
             (or (every? symbol? (take-nth 2 (second expr)))
                 (println "not a calculus expression: inappropriate left value in let (note that destructuring is not supported)"))
             (every? calculus-expr? (take-nth 2 (rest (second expr))))
             (every? calculus-expr? (drop 2 expr)))
        (and (= 'fn (first expr))
             (vector? (second expr))
             (or (every? symbol? (second expr))
                 (println "not a calculus expression: inappropriate function argument (note that destructuring is not supported)"))
             (every? calculus-expr? (drop 2 expr)))
        (and (not (calculus-keywords (first expr)))
             (every? calculus-expr? expr))))
    ;(println (str expr " is not a calculus expression"))
    ))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lambda-calculus/calculus-expr?</span>","value":"#'lambda-calculus/calculus-expr?"}
;; <=

;; **
;;; ---
;;; 
;;; #### About a-normal-form
;; **

;; @@
(def a-expr-keywords
  (-> calculus-keywords
      (dissoc 'let)
      (dissoc 'reduce 'map 'mapv)
      (assoc 'reduce* 3)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lambda-calculus/a-expr-keywords</span>","value":"#'lambda-calculus/a-expr-keywords"}
;; <=

;; @@
a-expr-keywords
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-map'>{</span>","close":"<span class='clj-map'>}</span>","separator":", ","items":[{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>conj</span>","value":"conj"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"}],"value":"[conj 2]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>discrete</span>","value":"discrete"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"}],"value":"[discrete 1]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>fn</span>","value":"fn"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"}],"value":"[fn 2]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>observe</span>","value":"observe"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"}],"value":"[observe 2]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>reduce*</span>","value":"reduce*"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"}],"value":"[reduce* 3]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>nth</span>","value":"nth"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"}],"value":"[nth 2]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>sample</span>","value":"sample"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"}],"value":"[sample 1]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>dirichlet</span>","value":"dirichlet"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"}],"value":"[dirichlet 1]"}],"value":"{conj 2, discrete 1, fn 2, observe 2, reduce* 3, nth 2, sample 1, dirichlet 1}"}
;; <=

;; @@
(declare e-expr?)

(defn a-expr? [expr]
  (or
    (symbol? expr)
    (constant? expr)
    (and (vector? expr) (every? symbol? expr))
    (and 
      (seq? expr)
      (every? symbol? expr)
      (or
        (= (get a-expr-keywords (first expr)) (dec (count expr)))
        (and (not (contains? special-keywords (first expr)))
             (= 2 (count expr)))))
    (and 
      (seq? expr)
      (= 'fn (first expr))
      (= 3 (count expr))
      (vector? (second expr))
      (= 1 (count (second expr)))
      (e-expr? (last expr)))))

(defn e-expr? [expr]  
  (and     
    (seq? expr)     
    (= 'let (first expr))    
    (= 3 (count expr))  
    (vector? (second expr))  
    (even? (count (second expr)))  
    (every? symbol? (take-nth 2 (second expr)))  
    (every? a-expr? (take-nth 2 (rest (second expr))))  
    (symbol? (last expr))))

(defn a-normal-form? [program]
  (e-expr? program))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lambda-calculus/a-normal-form?</span>","value":"#'lambda-calculus/a-normal-form?"}
;; <=

;; @@
(defn a-expr-kind [a]
  (cond
    (symbol? a) 'variable
    (constant? a) 'constant
    (vector? a) 'vector
    (contains? a-expr-keywords (first a)) (first a)
    (and (seq? a) (= 'sample-dirichlet (first a))) 'sample-dirichlet
    :else 'application))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lambda-calculus/a-expr-kind</span>","value":"#'lambda-calculus/a-expr-kind"}
;; <=

;; @@
(a-expr-kind '(a b))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-symbol'>application</span>","value":"application"}
;; <=

;; **
;;; ---
;;; 
;;; # Examples
;; **

;; @@
;; program

(calculus-expr?
  '(let [theta0 (sample (dirichlet alpha))
         theta1 (sample (dirichlet alpha))
         makephi (fn [] (sample (dirichlet beta-)))
         phi [(makephi) (makephi) (makephi)]
         f (fn [psi theta w] (let [z (sample (discrete theta))]
                               (observe (discrete (nth psi z)) w)
                               z))
         zs0 [(f phi theta0 w00)
              (f phi theta0 w01)
              (f phi theta0 w02)
              (f phi theta0 w03)]
         zs1 [(f phi theta1 w10)
              (f phi theta1 w11)
              (f phi theta1 w12)
              (f phi theta1 w13)]]
     [zs0 zs1]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>true</span>","value":"true"}
;; <=

;; @@
;; program-not-calculus-expr

;; returns false and prints:
;; "not a calculus expression: inappropriate left value in let (note that destructuring is not supported)"

;; commented because otherwise the message is printed when using this file from another one
;(calculus-expr?
  '(let [theta0 (sample (dirichlet alpha))
         theta1 (sample (dirichlet alpha))
         makephi (fn [] (sample (dirichlet beta-)))
         phi [(makephi) (makephi) (makephi)]
         f (fn [psi theta w] (let [z (sample (discrete theta))]
                               (observe (discrete (nth psi z)) w)
                               z))
         zs0 [(f phi theta0 w00)
              (f phi theta0 w01)
              (f phi theta0 w02)
              (f phi theta0 w03)]
         zs1 [(f phi theta1 w10)
              (f phi theta1 w11)
              (f phi theta1 w12)
              (f phi theta1 w13)]
         [a b c] alpha]
     [zs0 zs1])
;)
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>let</span>","value":"let"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>sample</span>","value":"sample"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>dirichlet</span>","value":"dirichlet"},{"type":"html","content":"<span class='clj-symbol'>alpha</span>","value":"alpha"}],"value":"(dirichlet alpha)"}],"value":"(sample (dirichlet alpha))"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>sample</span>","value":"sample"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>dirichlet</span>","value":"dirichlet"},{"type":"html","content":"<span class='clj-symbol'>alpha</span>","value":"alpha"}],"value":"(dirichlet alpha)"}],"value":"(sample (dirichlet alpha))"},{"type":"html","content":"<span class='clj-symbol'>makephi</span>","value":"makephi"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>fn</span>","value":"fn"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[],"value":"[]"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>sample</span>","value":"sample"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>dirichlet</span>","value":"dirichlet"},{"type":"html","content":"<span class='clj-symbol'>beta-</span>","value":"beta-"}],"value":"(dirichlet beta-)"}],"value":"(sample (dirichlet beta-))"}],"value":"(fn [] (sample (dirichlet beta-)))"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>makephi</span>","value":"makephi"}],"value":"(makephi)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>makephi</span>","value":"makephi"}],"value":"(makephi)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>makephi</span>","value":"makephi"}],"value":"(makephi)"}],"value":"[(makephi) (makephi) (makephi)]"},{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>fn</span>","value":"fn"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>psi</span>","value":"psi"},{"type":"html","content":"<span class='clj-symbol'>theta</span>","value":"theta"},{"type":"html","content":"<span class='clj-symbol'>w</span>","value":"w"}],"value":"[psi theta w]"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>let</span>","value":"let"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>z</span>","value":"z"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>sample</span>","value":"sample"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>discrete</span>","value":"discrete"},{"type":"html","content":"<span class='clj-symbol'>theta</span>","value":"theta"}],"value":"(discrete theta)"}],"value":"(sample (discrete theta))"}],"value":"[z (sample (discrete theta))]"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>observe</span>","value":"observe"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>discrete</span>","value":"discrete"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>nth</span>","value":"nth"},{"type":"html","content":"<span class='clj-symbol'>psi</span>","value":"psi"},{"type":"html","content":"<span class='clj-symbol'>z</span>","value":"z"}],"value":"(nth psi z)"}],"value":"(discrete (nth psi z))"},{"type":"html","content":"<span class='clj-symbol'>w</span>","value":"w"}],"value":"(observe (discrete (nth psi z)) w)"},{"type":"html","content":"<span class='clj-symbol'>z</span>","value":"z"}],"value":"(let [z (sample (discrete theta))] (observe (discrete (nth psi z)) w) z)"}],"value":"(fn [psi theta w] (let [z (sample (discrete theta))] (observe (discrete (nth psi z)) w) z))"},{"type":"html","content":"<span class='clj-symbol'>zs0</span>","value":"zs0"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"html","content":"<span class='clj-symbol'>w00</span>","value":"w00"}],"value":"(f phi theta0 w00)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"html","content":"<span class='clj-symbol'>w01</span>","value":"w01"}],"value":"(f phi theta0 w01)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"html","content":"<span class='clj-symbol'>w02</span>","value":"w02"}],"value":"(f phi theta0 w02)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"html","content":"<span class='clj-symbol'>w03</span>","value":"w03"}],"value":"(f phi theta0 w03)"}],"value":"[(f phi theta0 w00) (f phi theta0 w01) (f phi theta0 w02) (f phi theta0 w03)]"},{"type":"html","content":"<span class='clj-symbol'>zs1</span>","value":"zs1"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"html","content":"<span class='clj-symbol'>w10</span>","value":"w10"}],"value":"(f phi theta1 w10)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"html","content":"<span class='clj-symbol'>w11</span>","value":"w11"}],"value":"(f phi theta1 w11)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"html","content":"<span class='clj-symbol'>w12</span>","value":"w12"}],"value":"(f phi theta1 w12)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"html","content":"<span class='clj-symbol'>w13</span>","value":"w13"}],"value":"(f phi theta1 w13)"}],"value":"[(f phi theta1 w10) (f phi theta1 w11) (f phi theta1 w12) (f phi theta1 w13)]"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>a</span>","value":"a"},{"type":"html","content":"<span class='clj-symbol'>b</span>","value":"b"},{"type":"html","content":"<span class='clj-symbol'>c</span>","value":"c"}],"value":"[a b c]"},{"type":"html","content":"<span class='clj-symbol'>alpha</span>","value":"alpha"}],"value":"[theta0 (sample (dirichlet alpha)) theta1 (sample (dirichlet alpha)) makephi (fn [] (sample (dirichlet beta-))) phi [(makephi) (makephi) (makephi)] f (fn [psi theta w] (let [z (sample (discrete theta))] (observe (discrete (nth psi z)) w) z)) zs0 [(f phi theta0 w00) (f phi theta0 w01) (f phi theta0 w02) (f phi theta0 w03)] zs1 [(f phi theta1 w10) (f phi theta1 w11) (f phi theta1 w12) (f phi theta1 w13)] [a b c] alpha]"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>zs0</span>","value":"zs0"},{"type":"html","content":"<span class='clj-symbol'>zs1</span>","value":"zs1"}],"value":"[zs0 zs1]"}],"value":"(let [theta0 (sample (dirichlet alpha)) theta1 (sample (dirichlet alpha)) makephi (fn [] (sample (dirichlet beta-))) phi [(makephi) (makephi) (makephi)] f (fn [psi theta w] (let [z (sample (discrete theta))] (observe (discrete (nth psi z)) w) z)) zs0 [(f phi theta0 w00) (f phi theta0 w01) (f phi theta0 w02) (f phi theta0 w03)] zs1 [(f phi theta1 w10) (f phi theta1 w11) (f phi theta1 w12) (f phi theta1 w13)] [a b c] alpha] [zs0 zs1])"}
;; <=

;; @@
;; not calculus-expr

;; returns false and prints:
;; "not a calculus expression: inappropriate function argument (note that destructuring is not supported)"

;; commented because otherwise the message is printed when using this file from another one
;(calculus-expr?
  '(let [theta0 (sample (dirichlet alpha))
         theta1 (sample (dirichlet alpha))
         makephi (fn [[a b]] (sample (dirichlet beta-)))
         phi [(makephi) (makephi) (makephi)]
         f (fn [psi theta w] (let [z (sample (discrete theta))]
                               (observe (discrete (nth psi z)) w)
                               z))
         zs0 [(f phi theta0 w00)
              (f phi theta0 w01)
              (f phi theta0 w02)
              (f phi theta0 w03)]
         zs1 [(f phi theta1 w10)
              (f phi theta1 w11)
              (f phi theta1 w12)
              (f phi theta1 w13)]]
     [zs0 zs1])
;)
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>let</span>","value":"let"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>sample</span>","value":"sample"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>dirichlet</span>","value":"dirichlet"},{"type":"html","content":"<span class='clj-symbol'>alpha</span>","value":"alpha"}],"value":"(dirichlet alpha)"}],"value":"(sample (dirichlet alpha))"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>sample</span>","value":"sample"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>dirichlet</span>","value":"dirichlet"},{"type":"html","content":"<span class='clj-symbol'>alpha</span>","value":"alpha"}],"value":"(dirichlet alpha)"}],"value":"(sample (dirichlet alpha))"},{"type":"html","content":"<span class='clj-symbol'>makephi</span>","value":"makephi"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>fn</span>","value":"fn"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>a</span>","value":"a"},{"type":"html","content":"<span class='clj-symbol'>b</span>","value":"b"}],"value":"[a b]"}],"value":"[[a b]]"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>sample</span>","value":"sample"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>dirichlet</span>","value":"dirichlet"},{"type":"html","content":"<span class='clj-symbol'>beta-</span>","value":"beta-"}],"value":"(dirichlet beta-)"}],"value":"(sample (dirichlet beta-))"}],"value":"(fn [[a b]] (sample (dirichlet beta-)))"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>makephi</span>","value":"makephi"}],"value":"(makephi)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>makephi</span>","value":"makephi"}],"value":"(makephi)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>makephi</span>","value":"makephi"}],"value":"(makephi)"}],"value":"[(makephi) (makephi) (makephi)]"},{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>fn</span>","value":"fn"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>psi</span>","value":"psi"},{"type":"html","content":"<span class='clj-symbol'>theta</span>","value":"theta"},{"type":"html","content":"<span class='clj-symbol'>w</span>","value":"w"}],"value":"[psi theta w]"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>let</span>","value":"let"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>z</span>","value":"z"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>sample</span>","value":"sample"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>discrete</span>","value":"discrete"},{"type":"html","content":"<span class='clj-symbol'>theta</span>","value":"theta"}],"value":"(discrete theta)"}],"value":"(sample (discrete theta))"}],"value":"[z (sample (discrete theta))]"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>observe</span>","value":"observe"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>discrete</span>","value":"discrete"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>nth</span>","value":"nth"},{"type":"html","content":"<span class='clj-symbol'>psi</span>","value":"psi"},{"type":"html","content":"<span class='clj-symbol'>z</span>","value":"z"}],"value":"(nth psi z)"}],"value":"(discrete (nth psi z))"},{"type":"html","content":"<span class='clj-symbol'>w</span>","value":"w"}],"value":"(observe (discrete (nth psi z)) w)"},{"type":"html","content":"<span class='clj-symbol'>z</span>","value":"z"}],"value":"(let [z (sample (discrete theta))] (observe (discrete (nth psi z)) w) z)"}],"value":"(fn [psi theta w] (let [z (sample (discrete theta))] (observe (discrete (nth psi z)) w) z))"},{"type":"html","content":"<span class='clj-symbol'>zs0</span>","value":"zs0"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"html","content":"<span class='clj-symbol'>w00</span>","value":"w00"}],"value":"(f phi theta0 w00)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"html","content":"<span class='clj-symbol'>w01</span>","value":"w01"}],"value":"(f phi theta0 w01)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"html","content":"<span class='clj-symbol'>w02</span>","value":"w02"}],"value":"(f phi theta0 w02)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta0</span>","value":"theta0"},{"type":"html","content":"<span class='clj-symbol'>w03</span>","value":"w03"}],"value":"(f phi theta0 w03)"}],"value":"[(f phi theta0 w00) (f phi theta0 w01) (f phi theta0 w02) (f phi theta0 w03)]"},{"type":"html","content":"<span class='clj-symbol'>zs1</span>","value":"zs1"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"html","content":"<span class='clj-symbol'>w10</span>","value":"w10"}],"value":"(f phi theta1 w10)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"html","content":"<span class='clj-symbol'>w11</span>","value":"w11"}],"value":"(f phi theta1 w11)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"html","content":"<span class='clj-symbol'>w12</span>","value":"w12"}],"value":"(f phi theta1 w12)"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>f</span>","value":"f"},{"type":"html","content":"<span class='clj-symbol'>phi</span>","value":"phi"},{"type":"html","content":"<span class='clj-symbol'>theta1</span>","value":"theta1"},{"type":"html","content":"<span class='clj-symbol'>w13</span>","value":"w13"}],"value":"(f phi theta1 w13)"}],"value":"[(f phi theta1 w10) (f phi theta1 w11) (f phi theta1 w12) (f phi theta1 w13)]"}],"value":"[theta0 (sample (dirichlet alpha)) theta1 (sample (dirichlet alpha)) makephi (fn [[a b]] (sample (dirichlet beta-))) phi [(makephi) (makephi) (makephi)] f (fn [psi theta w] (let [z (sample (discrete theta))] (observe (discrete (nth psi z)) w) z)) zs0 [(f phi theta0 w00) (f phi theta0 w01) (f phi theta0 w02) (f phi theta0 w03)] zs1 [(f phi theta1 w10) (f phi theta1 w11) (f phi theta1 w12) (f phi theta1 w13)]]"},{"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>zs0</span>","value":"zs0"},{"type":"html","content":"<span class='clj-symbol'>zs1</span>","value":"zs1"}],"value":"[zs0 zs1]"}],"value":"(let [theta0 (sample (dirichlet alpha)) theta1 (sample (dirichlet alpha)) makephi (fn [[a b]] (sample (dirichlet beta-))) phi [(makephi) (makephi) (makephi)] f (fn [psi theta w] (let [z (sample (discrete theta))] (observe (discrete (nth psi z)) w) z)) zs0 [(f phi theta0 w00) (f phi theta0 w01) (f phi theta0 w02) (f phi theta0 w03)] zs1 [(f phi theta1 w10) (f phi theta1 w11) (f phi theta1 w12) (f phi theta1 w13)]] [zs0 zs1])"}
;; <=

;; @@
;; not calculus-expr because of (let a b) (no message associated)

(calculus-expr?
  '(let [theta0 (let a b)
         theta1 (sample (dirichlet alpha))
         makephi (fn [[a b]] (sample (dirichlet beta-)))
         phi [(makephi) (makephi) (makephi)]
         f (fn [psi theta w] (let [z (sample (discrete theta))]
                               (observe (discrete (nth psi z)) w)
                               z))
         zs0 [(f phi theta0 w00)
              (f phi theta0 w01)
              (f phi theta0 w02)
              (f phi theta0 w03)]
         zs1 [(f phi theta1 w10)
              (f phi theta1 w11)
              (f phi theta1 w12)
              (f phi theta1 w13)]]
     [zs0 zs1]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>false</span>","value":"false"}
;; <=

;; **
;;; ---
;; **

;; @@
;; program (not under a-normal form)

(a-normal-form?
  '(let [theta0 (sample (dirichlet alpha))
         theta1 (sample (dirichlet alpha))
         makephi (fn [] (sample (dirichlet beta-)))
         phi [(makephi) (makephi) (makephi)]
         f (fn [psi theta w] (let [z (sample (discrete theta))]
                               (observe (discrete (nth psi z)) w)
                               z))
         zs0 [(f phi theta0 w00)
              (f phi theta0 w01)
              (f phi theta0 w02)
              (f phi theta0 w03)]
         zs1 [(f phi theta1 w10)
              (f phi theta1 w11)
              (f phi theta1 w12)
              (f phi theta1 w13)]]
     [zs0 zs1]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>false</span>","value":"false"}
;; <=

;; @@
;; program under a-normal form

(calculus-expr?
  '(let [x19538 (dirichlet alpha) theta0 (sample x19538) x19539 (dirichlet alpha) theta1 (sample x19539) makephi (fn [_19540] (let [x19541 (dirichlet beta-) x19542 (sample x19541)] x19542)) x19546 nil x19543 (makephi x19546) x19547 nil x19544 (makephi x19547) x19548 nil x19545 (makephi x19548) phi [x19543 x19544 x19545] f (fn [psi] (let [x19555 (fn [theta] (let [x19554 (fn [w] (let [x19551 (discrete theta) z (sample x19551) x19553 (nth psi z) x19552 (discrete x19553) _19549 (observe x19552 w)] z))] x19554))] x19555)) x19560 (f phi) x19561 (x19560 theta0) x19556 (x19561 w00) x19562 (f phi) x19563 (x19562 theta0) x19557 (x19563 w01) x19564 (f phi) x19565 (x19564 theta0) x19558 (x19565 w02) x19566 (f phi) x19567 (x19566 theta0) x19559 (x19567 w03) zs0 [x19556 x19557 x19558 x19559] x19572 (f phi) x19573 (x19572 theta1) x19568 (x19573 w10) x19574 (f phi) x19575 (x19574 theta1) x19569 (x19575 w11) x19576 (f phi) x19577 (x19576 theta1) x19570 (x19577 w12) x19578 (f phi) x19579 (x19578 theta1) x19571 (x19579 w13) zs1 [x19568 x19569 x19570 x19571] x19580 [zs0 zs1]] x19580))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>true</span>","value":"true"}
;; <=

;; @@
;; program-map under a-normal form

(a-normal-form?
  '(let [x22290 0 ws_0 (nth corpus x22290) x22291 1 ws_1 (nth corpus x22291) x22292 (dirichlet alpha) theta_0 (sample x22292) x22293 (dirichlet alpha) theta_1 (sample x22293) makephi (fn [_] (let [x22294 (dirichlet beta*) x22295 (sample x22294)] x22295)) _ nil x22296 [_ _ _] x22299 (fn [acc22297] (let [x22303 (fn [x22298] (let [x22301 (makephi x22298) x22302 (conj acc22297 x22301)] x22302))] x22303)) x22300 [] phis (reduce* x22299 x22300 x22296) f (fn [phis] (let [x22310 (fn [theta] (let [x22309 (fn [w] (let [x22306 (discrete theta) z (sample x22306) x22308 (nth phis z) x22307 (discrete x22308) _22304 (observe x22307 w)] z))] x22309))] x22310)) x22316 (f phis) x22311 (x22316 theta_0) x22314 (fn [acc22312] (let [x22319 (fn [x22313] (let [x22317 (x22311 x22313) x22318 (conj acc22312 x22317)] x22318))] x22319)) x22315 [] zs_0 (reduce* x22314 x22315 ws_0) x22325 (f phis) x22320 (x22325 theta_1) x22323 (fn [acc22321] (let [x22328 (fn [x22322] (let [x22326 (x22320 x22322) x22327 (conj acc22321 x22326)] x22327))] x22328)) x22324 [] zs_1 (reduce* x22323 x22324 ws_1) z-corpus [zs_0 zs_1]] z-corpus))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>true</span>","value":"true"}
;; <=

;; gorilla-repl.fileformat = 1

;; **
;;; # S-distributions
;; **

;; @@
(use 'nstools.ns)
(ns+ S-distributions
  (:like anglican-user.worksheet))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; S-distributions are related to S-sample and S-observe functions which behave like sample and observe, exept that they also take and return a state.
;;; 
;;; S-sample and S-observe may also be applied to usual distributions, in which case they pass the state without modifying it and otherwise behave exactly like their usual counterpart.
;;; 
;;; S-discrete builds a usual discrete distribution when given the usual type of argument (sequential), and an S-distribution otherwise.
;;; 
;;; S-distributions are implemented as maps {:sample function :observe function}. Ideally I should write a macro similar to defdist, but I don't know how to do it and this is enough to experiment right now.
;; **

;; @@
(defn distribution? [d] (instance? anglican.runtime.distribution d))

(defn S-sample [d S]
  (if (distribution? d)
    [(sample d) S]
    ((:sample d) S)))
(defn S-observe [d v S]
  (if (distribution? d)
    [(observe d v) S]
    ((:observe d) v S)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;S-distributions/S-observe</span>","value":"#'S-distributions/S-observe"}
;; <=

;; @@
(defn marginalise [pi data S]
  (let [k (keyword (gensym (str pi "--")))]
    [k (assoc S k data)]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;S-distributions/marginalise</span>","value":"#'S-distributions/marginalise"}
;; <=

;; @@
(defn S-distribution [base-distribution distribution--update-S]
  (fn [& args]
    (if (keyword? (first args))
      {:sample 
       (fn [S] (let [[distribution update-S] (distribution--update-S (first args) S)
                     value (sample distribution)]
                 [value (update-S value)]))
       :observe
       (fn [value S] (let [[distribution update-S] (distribution--update-S (first args) S)]
                       [(observe distribution value) (update-S value)]))}
      (apply base-distribution args))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;S-distributions/S-distribution</span>","value":"#'S-distributions/S-distribution"}
;; <=

;; **
;;; ---
;;; 
;;; #### S-discrete
;; **

;; @@
(defn inc-nth [v n] (assoc v n (inc (get v n))))
(defn S-discrete--distribution--update-S
  "Used in S-discrete when the argument has been collapsed. Gets the corresponding distribution
  and a function to update the state."
  [k S]
  (let [p (get S k)]
    [(discrete p) (fn [value] (assoc S k (inc-nth p value)))]))

(def S-discrete (S-distribution discrete S-discrete--distribution--update-S))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;S-distributions/S-discrete</span>","value":"#'S-distributions/S-discrete"}
;; <=

;; @@

;; @@

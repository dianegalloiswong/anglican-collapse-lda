;; gorilla-repl.fileformat = 1

;; **
;;; # S-normal
;; **

;; @@
(use 'nstools.ns)
(ns+ S-normal
  (:like anglican-user.worksheet)
  (:use S-distributions))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; Normal distribution is a conjugate prior with a normal with known variance likelihood.
;;; 
;;; hyperparameters: mu-0 and sigma2-0
;;; also fixed: sigma2
;;; 
;;; mu ~ N(mu-0,sigma2-0)
;;; d = N(mu,sigma2)
;;; 
;;; x0, x1, ... ~ d
;;; 
;;; marginalise mu
;;; 
;;; relevant information to carry on: mu\* and sigma2\* (estimations of equivalent hyperparameters), sigma2 (unchanged, but has to be remembered)
;; **

;; @@
(defn S-normal--distribution--update-S [k S]
  (let [[mu* sigma2* sigma2] (get S k)]
    [(normal mu* (Math/sqrt (+ sigma2* sigma2)))
     (fn [value] (let [sigma2** (/ (+ (/ sigma2*) (/ sigma2)))
                       mu** (* (+ (/ mu* sigma2*) (/ value sigma2))
     sigma2**)]
                   (assoc S k [mu** sigma2** sigma2])))]))

(def S-normal (S-distribution normal S-normal--distribution--update-S))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;S-normal/S-normal</span>","value":"#'S-normal/S-normal"}
;; <=

;; **
;;; #### Naive program
;;; 
;;; inconsistent, mean varies between -1 and 2 in a few runs
;; **

;; @@
(let [mu-0 0
      sigma-0 1
      sigma 1
      mu (sample (normal mu-0 sigma-0))
      d (normal mu sigma)
      _ (observe d 2)
      _ (observe d 3)
      xs (repeatedly 1000 (fn [] (sample d)))]
  (plot/histogram xs :bins 20))
;; @@
;; =>
;;; {"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"cdd82310-1f25-410f-a183-03b216e2b232","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"cdd82310-1f25-410f-a183-03b216e2b232","field":"data.y"}}],"marks":[{"type":"line","from":{"data":"cdd82310-1f25-410f-a183-03b216e2b232"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"data":[{"name":"cdd82310-1f25-410f-a183-03b216e2b232","values":[{"x":-1.5316935066254862,"y":0},{"x":-1.2347447456765874,"y":3.0},{"x":-0.9377959847276887,"y":6.0},{"x":-0.6408472237787899,"y":9.0},{"x":-0.34389846282989106,"y":20.0},{"x":-0.046949701880992234,"y":24.0},{"x":0.2499990590679066,"y":58.0},{"x":0.5469478200168054,"y":64.0},{"x":0.8438965809657042,"y":95.0},{"x":1.140845341914603,"y":109.0},{"x":1.4377941028635017,"y":126.0},{"x":1.7347428638124005,"y":110.0},{"x":2.0316916247612995,"y":106.0},{"x":2.3286403857101985,"y":81.0},{"x":2.6255891466590975,"y":68.0},{"x":2.9225379076079965,"y":53.0},{"x":3.2194866685568955,"y":33.0},{"x":3.5164354295057945,"y":16.0},{"x":3.8133841904546935,"y":8.0},{"x":4.110332951403592,"y":7.0},{"x":4.407281712352491,"y":4.0},{"x":4.70423047330139,"y":0}]}],"width":400,"height":247.2187957763672,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"cdd82310-1f25-410f-a183-03b216e2b232\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"cdd82310-1f25-410f-a183-03b216e2b232\", :field \"data.y\"}}], :marks [{:type \"line\", :from {:data \"cdd82310-1f25-410f-a183-03b216e2b232\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :data [{:name \"cdd82310-1f25-410f-a183-03b216e2b232\", :values ({:x -1.5316935066254862, :y 0} {:x -1.2347447456765874, :y 3.0} {:x -0.9377959847276887, :y 6.0} {:x -0.6408472237787899, :y 9.0} {:x -0.34389846282989106, :y 20.0} {:x -0.046949701880992234, :y 24.0} {:x 0.2499990590679066, :y 58.0} {:x 0.5469478200168054, :y 64.0} {:x 0.8438965809657042, :y 95.0} {:x 1.140845341914603, :y 109.0} {:x 1.4377941028635017, :y 126.0} {:x 1.7347428638124005, :y 110.0} {:x 2.0316916247612995, :y 106.0} {:x 2.3286403857101985, :y 81.0} {:x 2.6255891466590975, :y 68.0} {:x 2.9225379076079965, :y 53.0} {:x 3.2194866685568955, :y 33.0} {:x 3.5164354295057945, :y 16.0} {:x 3.8133841904546935, :y 8.0} {:x 4.110332951403592, :y 7.0} {:x 4.407281712352491, :y 4.0} {:x 4.70423047330139, :y 0})}], :width 400, :height 247.2188, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}
;; <=

;; **
;;; #### Collapsed program
;;; 
;;; Mean between 1 and 2 most of the time, and almost always between 0.5 and 2.5. Moreover, printing the state we see the theoritical posterior distribution is N(1.67,1.33).
;; **

;; @@
(let [S {}
      mu-0 0.
      sigma-0 1.
      sigma 1.
      square (fn [x] (* x x))
      mu (sample (normal mu-0 sigma-0))
      [mu S] (marginalise "normal" [mu-0 (square sigma-0) (square sigma)] S)
      _ (println S)
      d (S-normal mu sigma)
      [_ S] (S-observe d 2 S)
      _ (println S)
      [_ S] (S-observe d 3 S)
      _ (println S)
      [xs S] (reduce (fn [[acc S] _] (let [[x S] (S-sample d S)] [(conj acc x) S])) [[] S] (range 1000))
      _ (println S)
      ]
  (plot/histogram xs :bins 20)
  )
;; @@
;; ->
;;; {:normal--21840 [0.0 1.0 1.0]}
;;; {:normal--21840 [1.0 0.5 1.0]}
;;; {:normal--21840 [1.6666666666666665 0.3333333333333333 1.0]}
;;; {:normal--21840 [1.3784750231123462 9.97008973080757E-4 1.0]}
;;; 
;; <-
;; =>
;;; {"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"7b6bcf2e-5e1b-42c0-8ba6-f11cc7072fb1","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"7b6bcf2e-5e1b-42c0-8ba6-f11cc7072fb1","field":"data.y"}}],"marks":[{"type":"line","from":{"data":"7b6bcf2e-5e1b-42c0-8ba6-f11cc7072fb1"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"data":[{"name":"7b6bcf2e-5e1b-42c0-8ba6-f11cc7072fb1","values":[{"x":-2.302968270246443,"y":0},{"x":-1.9521023871261873,"y":1.0},{"x":-1.6012365040059318,"y":2.0},{"x":-1.2503706208856762,"y":3.0},{"x":-0.8995047377654206,"y":12.0},{"x":-0.548638854645165,"y":16.0},{"x":-0.19777297152490947,"y":31.0},{"x":0.1530929115953461,"y":60.0},{"x":0.5039587947156017,"y":75.0},{"x":0.8548246778358572,"y":100.0},{"x":1.2056905609561128,"y":121.0},{"x":1.5565564440763684,"y":138.0},{"x":1.907422327196624,"y":137.0},{"x":2.2582882103168798,"y":118.0},{"x":2.6091540934371356,"y":66.0},{"x":2.9600199765573914,"y":60.0},{"x":3.310885859677647,"y":30.0},{"x":3.661751742797903,"y":19.0},{"x":4.012617625918159,"y":5.0},{"x":4.3634835090384145,"y":2.0},{"x":4.71434939215867,"y":4.0},{"x":5.065215275278926,"y":0}]}],"width":400,"height":247.2187957763672,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"7b6bcf2e-5e1b-42c0-8ba6-f11cc7072fb1\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"7b6bcf2e-5e1b-42c0-8ba6-f11cc7072fb1\", :field \"data.y\"}}], :marks [{:type \"line\", :from {:data \"7b6bcf2e-5e1b-42c0-8ba6-f11cc7072fb1\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :data [{:name \"7b6bcf2e-5e1b-42c0-8ba6-f11cc7072fb1\", :values ({:x -2.302968270246443, :y 0} {:x -1.9521023871261873, :y 1.0} {:x -1.6012365040059318, :y 2.0} {:x -1.2503706208856762, :y 3.0} {:x -0.8995047377654206, :y 12.0} {:x -0.548638854645165, :y 16.0} {:x -0.19777297152490947, :y 31.0} {:x 0.1530929115953461, :y 60.0} {:x 0.5039587947156017, :y 75.0} {:x 0.8548246778358572, :y 100.0} {:x 1.2056905609561128, :y 121.0} {:x 1.5565564440763684, :y 138.0} {:x 1.907422327196624, :y 137.0} {:x 2.2582882103168798, :y 118.0} {:x 2.6091540934371356, :y 66.0} {:x 2.9600199765573914, :y 60.0} {:x 3.310885859677647, :y 30.0} {:x 3.661751742797903, :y 19.0} {:x 4.012617625918159, :y 5.0} {:x 4.3634835090384145, :y 2.0} {:x 4.71434939215867, :y 4.0} {:x 5.065215275278926, :y 0})}], :width 400, :height 247.2188, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}
;; <=

;; @@

;; @@

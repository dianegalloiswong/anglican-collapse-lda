;; gorilla-repl.fileformat = 1

;; **
;;; # S-distributions 
;;; ### as defm to be used inside queries
;; **

;; @@
(use 'nstools.ns)
(ns+ S-distributions-in-query
  (:like anglican-user.worksheet))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; S-distributions are related to S-sample and S-observe functions which behave like sample and observe, exept that they also take and return a state.
;;; 
;;; S-sample and S-observe may also be applied to usual distributions, in which case they pass the state without modifying it and otherwise behave exactly like their usual counterpart.
;;; 
;;; S-discrete builds a usual discrete distribution when given the usual type of argument (sequential), and an S-distribution otherwise.
;;; 
;;; S-distributions are implemented as maps {:sample function :observe function}. Ideally I should write a macro similar to defdist, but I don't know how to do it and this is enough to experiment right now.
;; **

;; @@
(defm distribution? [d] (instance? anglican.runtime.distribution d))

(defm S-sample [d S]
  (if (distribution? d)
    [(sample d) S]
    ((:sample d) S)))
(defm S-observe [d v S]
  (if (distribution? d)
    [(observe d v) S]
    ((:observe d) v S)))



(defm inc-nth [v n] (assoc v n (inc (get v n))))
(defm S-discrete-dist-update
  "Used in S-discrete when the argument has been collapsed. Gets the corresponding distribution
  and a function to update the state."
  [k S]
  (let [p (get S k)]
    [(discrete p) (fn [value] (assoc S k (inc-nth p value)))]))

(defm S-discrete [p]
  (if (sequential? p)
    (discrete p)
    {:sample 
       (fn [S] (let [[dist update] (S-discrete-dist-update p S)
                     value (sample dist)]
                 [value (update value)]))
     :observe
       (fn [value S] (let [[dist update] (S-discrete-dist-update p S)]
                       [(observe dist value) (update value)]))}))

(defm marginalise [pi alpha S]
  (let [k (keyword (gensym (str pi "--")))]
    [k (assoc S k alpha)]))

(def marginalise-sample-dirichlet marginalise)
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;S-distributions-in-query/marginalise-sample-dirichlet</span>","value":"#'S-distributions-in-query/marginalise-sample-dirichlet"}
;; <=

;; @@
(defm reduce* [f init v S]
  (reduce 
    (fn [[acc S] x] ((first (f acc nil)) x S))
    [init S] v))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;S-distributions-in-query/reduce*</span>","value":"#'S-distributions-in-query/reduce*"}
;; <=

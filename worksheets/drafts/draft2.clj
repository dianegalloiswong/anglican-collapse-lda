;; gorilla-repl.fileformat = 1

;; **
;;; # Gorilla REPL
;;; 
;;; Welcome to gorilla :-)
;;; 
;;; Shift + enter evaluates code. Hit alt+g twice in quick succession or click the menu icon (upper-right corner) for more commands ...
;;; 
;;; It's a good habit to run each worksheet in its own namespace: feel free to use the declaration we've provided below if you'd like.
;; **

;; @@
(ns draft2
  (:require [gorilla-plot.core :as plot])
  (:use [embang runtime emit]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; Let's define a distribution:
;; **

;; @@
(defdist make-d [] (sample [this] 1) (observe [this value] 0))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>#&lt;MultiFn clojure.lang.MultiFn@1236ea6e&gt;</span>","value":"#<MultiFn clojure.lang.MultiFn@1236ea6e>"}
;; <=

;; **
;;; Now we can use it:
;; **

;; @@
(def d (make-d))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;draft2/d</span>","value":"#'draft2/d"}
;; <=

;; @@
d
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-map'>{</span>","close":"<span class='clj-map'>}</span>","separator":", ","items":[],"value":"(draft2/make-d)"}
;; <=

;; @@
(type d)
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-class'>draft2.make-d-distribution</span>","value":"draft2.make-d-distribution"}
;; <=

;; @@
(sample d)
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-long'>1</span>","value":"1"}
;; <=

;; @@
(observe d 1)
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-long'>0</span>","value":"0"}
;; <=

;; **
;;; Let's add two numbers:
;; **

;; @@
(+ 2 3)
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-long'>5</span>","value":"5"}
;; <=

;; **
;;; Let's plot a simple function.
;; **

;; @@
(plot/list-plot (map (partial + 2) (range 10)))
;; @@
;; =>
;;; {"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"63ddb6e6-b4c4-4fdb-8c08-943fc0a20546","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"63ddb6e6-b4c4-4fdb-8c08-943fc0a20546","field":"data.y"}}],"marks":[{"type":"symbol","from":{"data":"63ddb6e6-b4c4-4fdb-8c08-943fc0a20546"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"fill":{"value":"steelblue"},"fillOpacity":{"value":1}},"update":{"shape":"circle","size":{"value":70},"stroke":{"value":"transparent"}},"hover":{"size":{"value":210},"stroke":{"value":"white"}}}}],"data":[{"name":"63ddb6e6-b4c4-4fdb-8c08-943fc0a20546","values":[{"x":0,"y":2},{"x":1,"y":3},{"x":2,"y":4},{"x":3,"y":5},{"x":4,"y":6},{"x":5,"y":7},{"x":6,"y":8},{"x":7,"y":9},{"x":8,"y":10},{"x":9,"y":11}]}],"width":400,"height":247.2187957763672,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"63ddb6e6-b4c4-4fdb-8c08-943fc0a20546\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"63ddb6e6-b4c4-4fdb-8c08-943fc0a20546\", :field \"data.y\"}}], :marks [{:type \"symbol\", :from {:data \"63ddb6e6-b4c4-4fdb-8c08-943fc0a20546\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 1}}, :update {:shape \"circle\", :size {:value 70}, :stroke {:value \"transparent\"}}, :hover {:size {:value 210}, :stroke {:value \"white\"}}}}], :data [{:name \"63ddb6e6-b4c4-4fdb-8c08-943fc0a20546\", :values ({:x 0, :y 2} {:x 1, :y 3} {:x 2, :y 4} {:x 3, :y 5} {:x 4, :y 6} {:x 5, :y 7} {:x 6, :y 8} {:x 7, :y 9} {:x 8, :y 10} {:x 9, :y 11})}], :width 400, :height 247.2188, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}
;; <=

;; @@

;; @@

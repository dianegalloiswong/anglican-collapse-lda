;; gorilla-repl.fileformat = 1

;; **
;;; Boiler-plate code --- importing necessary things.
;; **

;; @@
(use 'nstools.ns)
(ns+ dirichlet
  (:like anglican-user.worksheet))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; Let us learn a categorical distribution with a dirichlet prior, using two different methods.
;; **

;; **
;;; We generate such a distribution and training data for it.
;; **

;; @@
(def alpha (repeat 10 1))
(def p (sample (dirichlet alpha)))
p
(def training (repeatedly #(sample (discrete p))))
(take 20 training)
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-long'>7</span>","value":"7"},{"type":"html","content":"<span class='clj-long'>7</span>","value":"7"},{"type":"html","content":"<span class='clj-long'>7</span>","value":"7"},{"type":"html","content":"<span class='clj-long'>6</span>","value":"6"},{"type":"html","content":"<span class='clj-long'>7</span>","value":"7"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>6</span>","value":"6"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"},{"type":"html","content":"<span class='clj-long'>7</span>","value":"7"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"},{"type":"html","content":"<span class='clj-long'>7</span>","value":"7"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>7</span>","value":"7"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>7</span>","value":"7"}],"value":"(7 7 7 6 7 1 6 1 1 4 7 3 7 1 7 1 9 1 1 7)"}
;; <=

;; **
;;; First method: a lot of observes.
;; **

;; @@
(defquery dirichlet-observes
  (let [p (sample (dirichlet alpha))
        d (discrete p)]
    (reduce (fn [_ x] (observe d x)) nil (take 10000 training))
    (predict 'x (sample d))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/dirichlet-observes</span>","value":"#'dirichlet/dirichlet-observes"}
;; <=

;; @@
(def samples-observes (doquery :lmh dirichlet-observes nil))
(def xs-observes (map #(get % 'x) (map get-predicts samples-observes)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/xs-observes</span>","value":"#'dirichlet/xs-observes"}
;; <=

;; **
;;; Second method: dirichlet process.
;; **

;; @@
(defproc DSD*
  "discrete-symmetric-dirichlet process"
  [alpha] [counts (vec alpha)]
  (produce [this] (discrete counts))
  (absorb [this sample]
    (DSD* alpha (update-in counts [sample] + 1.))))
(with-primitive-procedures [DSD*]
  (defm DSD 
    "discrete-symmetric-dirichlet process"
    [& args] (apply DSD* args)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/DSD</span>","value":"#'dirichlet/DSD"}
;; <=

;; @@
(defquery dirichlet-process
  (loop [proc (DSD alpha)
         data (take 1000 training)]
    (if (seq data)
      (let [[x & data-] data]
        (recur (absorb proc x) data-))
      (predict 'x (sample (produce proc))))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/dirichlet-process</span>","value":"#'dirichlet/dirichlet-process"}
;; <=

;; @@
(def samples-process (doquery :lmh dirichlet-process nil))
(def xs-process (map #(get % 'x) (map get-predicts samples-process)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/xs-process</span>","value":"#'dirichlet/xs-process"}
;; <=

;; **
;;; 
;; **

;; **
;;; Results: we compute the frequencies of each point over a 1000 samples from the original distribution or from the ones we have learnt. Unsurprisingly, the second method seems better than the first one, although the second method uses only 1000 training points (stack overflow with 10000) while the first method uses 10000.
;; **

;; @@
(defn frequencies-list [xs]
  (map #(get (frequencies xs) %) (range 10)))
(frequencies-list (repeatedly 1000 #(sample (discrete p))))
(frequencies-list (take 1000 xs-observes))
(frequencies-list (take 1000 xs-process))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-long'>53</span>","value":"53"},{"type":"html","content":"<span class='clj-long'>304</span>","value":"304"},{"type":"html","content":"<span class='clj-long'>28</span>","value":"28"},{"type":"html","content":"<span class='clj-long'>121</span>","value":"121"},{"type":"html","content":"<span class='clj-long'>10</span>","value":"10"},{"type":"html","content":"<span class='clj-long'>42</span>","value":"42"},{"type":"html","content":"<span class='clj-long'>83</span>","value":"83"},{"type":"html","content":"<span class='clj-long'>292</span>","value":"292"},{"type":"html","content":"<span class='clj-long'>32</span>","value":"32"},{"type":"html","content":"<span class='clj-long'>35</span>","value":"35"}],"value":"(53 304 28 121 10 42 83 292 32 35)"}
;; <=

;; @@

;; @@

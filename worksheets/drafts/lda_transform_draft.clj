;; gorilla-repl.fileformat = 1

;; **
;;; Boiler-plate code --- importing necessary things.
;; **

;; @@
(use 'nstools.ns)
(ns+ lda-transform-draft
  (:like anglican-user.worksheet))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; Let us consider a small Topic Model problem, and manually transform some code for a corresponding Latent Dirichlet Allocation.
;; **

;; **
;;; Parameters:
;; **

;; @@
(def D 2) ; number of documents
(def K 3) ; number of topics
(def N 100) ; number of words per document
(def V 10) ; number of words in the vocabulary
(def alpha (vec (repeat K 1))) ; hyperparameter for the distribution over topics for each document
(def beta* (vec (repeat V 0.1))) ; hyperparameter for the distibution over words for each topic
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/beta*</span>","value":"#'lda-transform-draft/beta*"}
;; <=

;; **
;;; We write a function to generate data according to these parameters.
;; **

;; @@
(defn vec* "transforms every nested sequential thing into a vector" [s]
  (if (sequential? s) (mapv vec* s) s))
(defn generate-data []
  (let [phis (repeatedly K #(sample (dirichlet beta*)))
        thetas (repeatedly D #(sample (dirichlet alpha)))
        corpus (for [d (range D)
                     :let [disc-theta (discrete (nth thetas d))]]
                 (repeatedly N #(sample (discrete (nth phis (sample disc-theta))))))]
    (vec* [:phis phis :thetas thetas :corpus corpus])))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/generate-data</span>","value":"#'lda-transform-draft/generate-data"}
;; <=

;; @@
;(generate-data)
;; @@

;; **
;;; The following has been obtained through a call to generate-data. We fix a corpus which will not be accidentally reset.
;; **

;; @@
(def actual-phis 
  [[0.040309461144695614 8.908530764161071E-8 4.7319026641838677E-7 0.31182101996778805 5.889922774241026E-6 0.25662483839799105 0.0035018305824863457 0.18352646625552543 0.08555541882014375 0.11865451263302157] [7.488849353792042E-4 0.0019690380225894366 0.3860855715920722 2.833864810858225E-6 8.612479628355917E-4 3.414401702107403E-14 0.0814882826179895 0.0024035444830440107 2.3265896780240306E-5 0.5264173306244647] [0.2199953880189719 0.25855355423611315 2.2293803776925123E-8 0.5138205618986098 2.938917194280698E-5 1.269361148933638E-4 0.0030440772615424974 8.164025619070032E-7 1.2087777308952904E-15 0.004429254601559448]])
(def actual-thetas 
  [[0.19139866454333346 0.6886302415407713 0.11997109391589533] [0.8538045032367265 0.0404092367327455 0.10578626003052798]])
(def corpus 
  [[2 3 2 9 5 2 9 9 9 8 9 2 3 6 9 9 9 5 9 9 9 9 9 2 1 9 9 9 6 2 9 9 9 9 9 3 6 9 9 9 7 2 9 9 9 0 9 2 2 2 3 9 2 3 9 9 2 9 9 2 9 9 9 0 2 9 7 9 1 2 2 3 2 3 7 9 2 9 9 2 9 2 3 8 8 9 9 9 7 9 2 9 5 9 9 3 6 2 9 2] [3 3 5 7 5 5 3 9 2 7 8 3 7 0 3 2 8 5 3 9 9 3 3 5 3 7 5 5 3 7 5 7 7 8 3 5 7 3 7 9 5 0 5 5 3 9 5 3 1 3 5 5 3 7 9 3 5 3 3 5 3 5 6 9 9 8 3 5 2 7 3 5 8 3 0 9 3 9 0 3 7 0 1 1 3 7 9 3 7 3 5 5 7 5 7 3 0 8 8 5]])
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/corpus</span>","value":"#'lda-transform-draft/corpus"}
;; <=

;; **
;;; Let us have a closer look to our corpus, with the discrete distibutions which have generated it.
;; **

;; **
;;; Content of each document (number of occurrences of each word): ws_0, ws_1
;; **

;; @@
(defn proportions
  "xs should be a sequence of integers in (range n). Returns a sequence where the element with index k is the proportion of elements of xs equal to k."
  [n xs]
  (map #(/ (get (frequencies xs) % 0) (count xs)) (range n)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/proportions</span>","value":"#'lda-transform-draft/proportions"}
;; <=

;; @@
(defn plot-corpus [corpus]
  (map (fn [ws] (plot/bar-chart (range V) (proportions V ws) :plot-size 100))
       corpus))
(plot-corpus corpus)
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"3755a791-951b-4699-a724-0b6aca207666","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"3755a791-951b-4699-a724-0b6aca207666","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"3755a791-951b-4699-a724-0b6aca207666"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"3755a791-951b-4699-a724-0b6aca207666","values":[{"x":0,"y":0.02},{"x":1,"y":0.02},{"x":2,"y":0.23},{"x":3,"y":0.09},{"x":4,"y":0},{"x":5,"y":0.03},{"x":6,"y":0.04},{"x":7,"y":0.04},{"x":8,"y":0.03},{"x":9,"y":0.5}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"3755a791-951b-4699-a724-0b6aca207666\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"3755a791-951b-4699-a724-0b6aca207666\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"3755a791-951b-4699-a724-0b6aca207666\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"3755a791-951b-4699-a724-0b6aca207666\", :values ({:x 0, :y 1/50} {:x 1, :y 1/50} {:x 2, :y 23/100} {:x 3, :y 9/100} {:x 4, :y 0} {:x 5, :y 3/100} {:x 6, :y 1/25} {:x 7, :y 1/25} {:x 8, :y 3/100} {:x 9, :y 1/2})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"ee43f366-0f4d-4de9-9fe9-519a72d4a094","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"ee43f366-0f4d-4de9-9fe9-519a72d4a094","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"ee43f366-0f4d-4de9-9fe9-519a72d4a094"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"ee43f366-0f4d-4de9-9fe9-519a72d4a094","values":[{"x":0,"y":0.06},{"x":1,"y":0.03},{"x":2,"y":0.03},{"x":3,"y":0.29},{"x":4,"y":0},{"x":5,"y":0.24},{"x":6,"y":0.01},{"x":7,"y":0.16},{"x":8,"y":0.07},{"x":9,"y":0.11}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"ee43f366-0f4d-4de9-9fe9-519a72d4a094\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"ee43f366-0f4d-4de9-9fe9-519a72d4a094\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"ee43f366-0f4d-4de9-9fe9-519a72d4a094\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"ee43f366-0f4d-4de9-9fe9-519a72d4a094\", :values ({:x 0, :y 3/50} {:x 1, :y 3/100} {:x 2, :y 3/100} {:x 3, :y 29/100} {:x 4, :y 0} {:x 5, :y 6/25} {:x 6, :y 1/100} {:x 7, :y 4/25} {:x 8, :y 7/100} {:x 9, :y 11/100})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}],"value":"(#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"3755a791-951b-4699-a724-0b6aca207666\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"3755a791-951b-4699-a724-0b6aca207666\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"3755a791-951b-4699-a724-0b6aca207666\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"3755a791-951b-4699-a724-0b6aca207666\", :values ({:x 0, :y 1/50} {:x 1, :y 1/50} {:x 2, :y 23/100} {:x 3, :y 9/100} {:x 4, :y 0} {:x 5, :y 3/100} {:x 6, :y 1/25} {:x 7, :y 1/25} {:x 8, :y 3/100} {:x 9, :y 1/2})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"ee43f366-0f4d-4de9-9fe9-519a72d4a094\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"ee43f366-0f4d-4de9-9fe9-519a72d4a094\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"ee43f366-0f4d-4de9-9fe9-519a72d4a094\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"ee43f366-0f4d-4de9-9fe9-519a72d4a094\", :values ({:x 0, :y 3/50} {:x 1, :y 3/100} {:x 2, :y 3/100} {:x 3, :y 29/100} {:x 4, :y 0} {:x 5, :y 6/25} {:x 6, :y 1/100} {:x 7, :y 4/25} {:x 8, :y 7/100} {:x 9, :y 11/100})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}})"}
;; <=

;; **
;;; Distribution over topics for each document: theta_0, theta_1
;; **

;; @@
(defn plot-thetas [thetas] (map #(plot/bar-chart (range K) % :plot-size 100) thetas))
(plot-thetas actual-thetas)
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316","values":[{"x":0,"y":0.19139866454333346},{"x":1,"y":0.6886302415407713},{"x":2,"y":0.11997109391589533}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316\", :values ({:x 0, :y 0.19139866454333346} {:x 1, :y 0.6886302415407713} {:x 2, :y 0.11997109391589533})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"c55c3771-8715-4adc-b122-cc63e4da15bd","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"c55c3771-8715-4adc-b122-cc63e4da15bd","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"c55c3771-8715-4adc-b122-cc63e4da15bd"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"c55c3771-8715-4adc-b122-cc63e4da15bd","values":[{"x":0,"y":0.8538045032367265},{"x":1,"y":0.0404092367327455},{"x":2,"y":0.10578626003052798}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"c55c3771-8715-4adc-b122-cc63e4da15bd\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"c55c3771-8715-4adc-b122-cc63e4da15bd\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"c55c3771-8715-4adc-b122-cc63e4da15bd\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"c55c3771-8715-4adc-b122-cc63e4da15bd\", :values ({:x 0, :y 0.8538045032367265} {:x 1, :y 0.0404092367327455} {:x 2, :y 0.10578626003052798})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}],"value":"(#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"a1ddcb7e-d7b3-4d06-8f5e-9656d7e68316\", :values ({:x 0, :y 0.19139866454333346} {:x 1, :y 0.6886302415407713} {:x 2, :y 0.11997109391589533})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"c55c3771-8715-4adc-b122-cc63e4da15bd\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"c55c3771-8715-4adc-b122-cc63e4da15bd\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"c55c3771-8715-4adc-b122-cc63e4da15bd\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"c55c3771-8715-4adc-b122-cc63e4da15bd\", :values ({:x 0, :y 0.8538045032367265} {:x 1, :y 0.0404092367327455} {:x 2, :y 0.10578626003052798})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}})"}
;; <=

;; **
;;; Distribution over words for each topic: phi_0, phi_1, phi_2
;; **

;; @@
(defn plot-phis [phis] (map #(plot/bar-chart (range V) % :plot-size 100) phis))
(plot-phis actual-phis)
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"7bf929dc-41b0-456f-b137-4d467a080cf0","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"7bf929dc-41b0-456f-b137-4d467a080cf0","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"7bf929dc-41b0-456f-b137-4d467a080cf0"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"7bf929dc-41b0-456f-b137-4d467a080cf0","values":[{"x":0,"y":0.040309461144695614},{"x":1,"y":8.908530764161071E-8},{"x":2,"y":4.7319026641838677E-7},{"x":3,"y":0.31182101996778805},{"x":4,"y":5.889922774241026E-6},{"x":5,"y":0.25662483839799105},{"x":6,"y":0.0035018305824863457},{"x":7,"y":0.18352646625552543},{"x":8,"y":0.08555541882014375},{"x":9,"y":0.11865451263302157}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"7bf929dc-41b0-456f-b137-4d467a080cf0\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"7bf929dc-41b0-456f-b137-4d467a080cf0\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"7bf929dc-41b0-456f-b137-4d467a080cf0\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"7bf929dc-41b0-456f-b137-4d467a080cf0\", :values ({:x 0, :y 0.040309461144695614} {:x 1, :y 8.908530764161071E-8} {:x 2, :y 4.7319026641838677E-7} {:x 3, :y 0.31182101996778805} {:x 4, :y 5.889922774241026E-6} {:x 5, :y 0.25662483839799105} {:x 6, :y 0.0035018305824863457} {:x 7, :y 0.18352646625552543} {:x 8, :y 0.08555541882014375} {:x 9, :y 0.11865451263302157})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd","values":[{"x":0,"y":7.488849353792042E-4},{"x":1,"y":0.0019690380225894366},{"x":2,"y":0.3860855715920722},{"x":3,"y":2.833864810858225E-6},{"x":4,"y":8.612479628355917E-4},{"x":5,"y":3.414401702107403E-14},{"x":6,"y":0.0814882826179895},{"x":7,"y":0.0024035444830440107},{"x":8,"y":2.3265896780240306E-5},{"x":9,"y":0.5264173306244647}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd\", :values ({:x 0, :y 7.488849353792042E-4} {:x 1, :y 0.0019690380225894366} {:x 2, :y 0.3860855715920722} {:x 3, :y 2.833864810858225E-6} {:x 4, :y 8.612479628355917E-4} {:x 5, :y 3.414401702107403E-14} {:x 6, :y 0.0814882826179895} {:x 7, :y 0.0024035444830440107} {:x 8, :y 2.3265896780240306E-5} {:x 9, :y 0.5264173306244647})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"24a3c9c0-3fbd-4a23-9a90-778ba7482380","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"24a3c9c0-3fbd-4a23-9a90-778ba7482380","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"24a3c9c0-3fbd-4a23-9a90-778ba7482380"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"24a3c9c0-3fbd-4a23-9a90-778ba7482380","values":[{"x":0,"y":0.2199953880189719},{"x":1,"y":0.25855355423611315},{"x":2,"y":2.2293803776925123E-8},{"x":3,"y":0.5138205618986098},{"x":4,"y":2.938917194280698E-5},{"x":5,"y":1.269361148933638E-4},{"x":6,"y":0.0030440772615424974},{"x":7,"y":8.164025619070032E-7},{"x":8,"y":1.2087777308952904E-15},{"x":9,"y":0.004429254601559448}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"24a3c9c0-3fbd-4a23-9a90-778ba7482380\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"24a3c9c0-3fbd-4a23-9a90-778ba7482380\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"24a3c9c0-3fbd-4a23-9a90-778ba7482380\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"24a3c9c0-3fbd-4a23-9a90-778ba7482380\", :values ({:x 0, :y 0.2199953880189719} {:x 1, :y 0.25855355423611315} {:x 2, :y 2.2293803776925123E-8} {:x 3, :y 0.5138205618986098} {:x 4, :y 2.938917194280698E-5} {:x 5, :y 1.269361148933638E-4} {:x 6, :y 0.0030440772615424974} {:x 7, :y 8.164025619070032E-7} {:x 8, :y 1.2087777308952904E-15} {:x 9, :y 0.004429254601559448})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}],"value":"(#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"7bf929dc-41b0-456f-b137-4d467a080cf0\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"7bf929dc-41b0-456f-b137-4d467a080cf0\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"7bf929dc-41b0-456f-b137-4d467a080cf0\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"7bf929dc-41b0-456f-b137-4d467a080cf0\", :values ({:x 0, :y 0.040309461144695614} {:x 1, :y 8.908530764161071E-8} {:x 2, :y 4.7319026641838677E-7} {:x 3, :y 0.31182101996778805} {:x 4, :y 5.889922774241026E-6} {:x 5, :y 0.25662483839799105} {:x 6, :y 0.0035018305824863457} {:x 7, :y 0.18352646625552543} {:x 8, :y 0.08555541882014375} {:x 9, :y 0.11865451263302157})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"ac7a5527-0cd8-4942-aeb6-f0eb016d40dd\", :values ({:x 0, :y 7.488849353792042E-4} {:x 1, :y 0.0019690380225894366} {:x 2, :y 0.3860855715920722} {:x 3, :y 2.833864810858225E-6} {:x 4, :y 8.612479628355917E-4} {:x 5, :y 3.414401702107403E-14} {:x 6, :y 0.0814882826179895} {:x 7, :y 0.0024035444830440107} {:x 8, :y 2.3265896780240306E-5} {:x 9, :y 0.5264173306244647})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"24a3c9c0-3fbd-4a23-9a90-778ba7482380\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"24a3c9c0-3fbd-4a23-9a90-778ba7482380\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"24a3c9c0-3fbd-4a23-9a90-778ba7482380\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"24a3c9c0-3fbd-4a23-9a90-778ba7482380\", :values ({:x 0, :y 0.2199953880189719} {:x 1, :y 0.25855355423611315} {:x 2, :y 2.2293803776925123E-8} {:x 3, :y 0.5138205618986098} {:x 4, :y 2.938917194280698E-5} {:x 5, :y 1.269361148933638E-4} {:x 6, :y 0.0030440772615424974} {:x 7, :y 8.164025619070032E-7} {:x 8, :y 1.2087777308952904E-15} {:x 9, :y 0.004429254601559448})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}})"}
;; <=

;; **
;;; ---
;; **

;; **
;;; ##Code transformation
;;; 
;;; The following code is a usual implementation of lda. However inference algorithms are not very efficient on this. We try to transform it to obtain better code regarding the inference algorithms. We do it manually, but through small steps which we hope to be quite easily computable.
;; **

;; @@
(defquery lda-initial
  (let [[ws_0 ws_1] corpus
        
        ;; initializing
        theta_0 (sample (dirichlet alpha))
        theta_1 (sample (dirichlet alpha))
        phis (repeatedly K (fn [] (sample (dirichlet beta*))))
        
        ;; behavior for each word, depending on some parameters
        ;; this function definition does not depend on initial samplings!
        f (fn [phis theta] (fn [w] (let [z (sample (discrete theta))]
                                     (observe (discrete (nth phis z)) w)
                                     z)))
        
        ;; applying it to each word of each document, one parameter depends on the document
        zs_0 (map (f phis theta_0) ws_0)
        zs_1 (map (f phis theta_1) ws_1)
        
        z-corpus [zs_0 zs_1]]
    (predict z-corpus)
    
    ;; additional information which is not part of the code we want to transform, but may be interesting
    (predict phis)
    (predict 'thetas [theta_0 theta_1])))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/lda-initial</span>","value":"#'lda-transform-draft/lda-initial"}
;; <=

;; **
;;; ####Step 1 --- Add a state
;;; 
;;; Have a state be passed through loops (right now the state does nothing exept being passed).
;; **

;; @@
(defquery lda-step1
  (let [[ws_0 ws_1] corpus
        
        ;; initializing
        state {}
        theta_0 (sample (dirichlet alpha))
        theta_1 (sample (dirichlet alpha))
        phis (repeatedly K (fn [] (sample (dirichlet beta*))))
        state {}
        
        ;; behavior for each word, depending on some parameters
        ;; this function definition does not depend on initial samplings!
        f (fn [phis theta] (fn [[state acc] w] (let [z (sample (discrete theta))]
                                                 (observe (discrete (nth phis z)) w)
                                                 [state (conj acc z)])))
        
        ;; applying it to each word of each document, one parameter depends on the document
        [state zs_0] (reduce (f phis theta_0) [state []] ws_0)
        [state zs_1] (reduce (f phis theta_1) [state []] ws_1)
        
        z-corpus [zs_0 zs_1]]
    (predict z-corpus)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/lda-step1</span>","value":"#'lda-transform-draft/lda-step1"}
;; <=

;; **
;;; ####Step2 --- Marginalize over theta_0
;;; 
;;; Conditions allowing it:
;;; 1. theta_0 is sampled from a Dirichlet distribution
;;; 2. theta_0 only appears as an argument for a discrete distribution.
;;; Note: theta_0 is used in a call to f, but this is fine since the argument theta of f follows this condition in the body of f.
;;; 3. The Dirichlet distribution is a conjugate prior with a discrete likelihood.
;;; 
;;; Corresponding code transformation:
;;; 
;;; We use a finite Dirichlet process, the relevant information being stored in the state. The variable theta_0 now contains a keyword which grants access to this information. 
;;; See comments on S-distributions below.
;;; The two behaviors availability is needed because in calls to f, argument theta is sometimes theta_0, but not always.
;;; 
;; **

;; @@
;; S-distributions are related to sample and observe functions which behave like those of a usual distribution, exept that they also take and return a state.
;; An S-distribution may be defined from the same arguments as the corresponding usual distribution, in which case the functions leave the state unchanged and otherwise have the usual behavior. Or it may be defined from arguments of a different type, in which case they are the keys to information stored in the state which is relevant to a dynamic process.
;; S-distributions are implemented as maps {:sample <function> :observe <function>}. Ideally I should write a macro similar to defdist, but I don't know how to do it and this is enough to experiment right now.

(defm S-sample [state S-dist] ((get S-dist :sample) state))
(defm S-observe [state S-dist value] ((get S-dist :observe) state value))

(defm inc-nth [v n] (assoc v n (inc (get v n))))
(defm S-discrete-utils
  "Used in S-discrete. Gets the underlying usual distribution
  and a function to update the state."
  [state p]
  (if (sequential? p)
    [(discrete p)
     (fn [value] state)]
    (let [p* (get state p)]
      [(discrete p*)
       (fn [value] (assoc state p (inc-nth p* value)))])))

(defm S-discrete
  "Creates an S-distribution corresponding to a discrete distribution."
  [p]
  {:sample (fn [state] (let [[dist update-state] (S-discrete-utils state p)
                             value (sample dist)]
                         [(update-state value) value]))
   :observe (fn [state value] (let [[dist update-state] (S-discrete-utils state p)]
                                [(update-state value) (observe dist value)]))})

(defn init-DP*
  ([state alpha] (init-DP* state alpha (gensym "DPvec")))
  ([state alpha key] [(assoc state key alpha) key]))
(with-primitive-procedures [init-DP*]
  (defm init-DP [& args] (apply init-DP* args)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/init-DP</span>","value":"#'lda-transform-draft/init-DP"}
;; <=

;; **
;;; ;; testing S-discrete, S-sample, S-observe, init-DP
;;; 
;;; (defquery tests
;;;   (let [state {}
;;;         theta (sample (dirichlet [1 1]))
;;;         theta1 (sample (dirichlet [1 1]))
;;;         [state theta2] (init-DP state [1 1] 'theta2)
;;;         d (discrete theta)
;;;         sd1 (S-discrete theta1)
;;;         sd2 (S-discrete theta2)
;;;         _ (observe d 0)
;;;         _ (observe d 0)
;;;         x (sample d)
;;;         [state _] (S-observe state sd1 0)
;;;         [state _] (S-observe state sd1 0)
;;;         [state x1] (S-sample state sd1)
;;;         [state _] (S-observe state sd2 0)
;;;         [state _] (S-observe state sd2 0)
;;;         [state x2] (S-sample state sd2)]
;;;     (predict x)
;;;     (predict x1)
;;;     (predict x2)
;;;     (predict state)))
;;; (->> (doquery :lmh tests nil) (take 10000) (map get-predicts) (map #(get % 'state)) (frequencies))
;; **

;; @@
(defquery lda-step2
  (let [[ws_0 ws_1] corpus
        
        ;; initializing
        state {}
        [state theta_0] (init-DP state alpha 'theta_0)
        theta_1 (sample (dirichlet alpha))
        phis (repeatedly K (fn [] (sample (dirichlet beta*))))
        _ (println state)
        
        ;; behavior for each word, depending on some parameters
        ;; this function definition does not depend on initial samplings!
        f (fn [phis theta] (fn [[state acc] w] (let [[state z] (S-sample state (S-discrete theta))]
                                                 (observe (discrete (nth phis z)) w)
                                                 [state (conj acc z)])))
        
        ;; applying it to each word of each document, one parameter depends on the document
        [state zs_0] (reduce (f phis theta_0) [state []] ws_0)
        _ (println state)
        [state zs_1] (reduce (f phis theta_1) [state []] ws_1)
        
        z-corpus [zs_0 zs_1]]
    (predict z-corpus)))
(def samples-step2 (doquery :lmh lda-step2 nil))
;; @@
;; ->
;;; {theta_0 [1 1 1]}
;;; {theta_0 [46 6 51]}
;;; 
;; <-
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/samples-step2</span>","value":"#'lda-transform-draft/samples-step2"}
;; <=

;; **
;;; ####Step 3 --- Marginalize over theta_1
;;; 
;;; Likewise marginalizing over theta_0.
;;; This is easy now because most of the work has already been done for theta_0. Only need to change the definition of theta_1.
;; **

;; @@
(defquery lda-step3
  (let [[ws_0 ws_1] corpus
        
        ;; initializing
        state {}
        [state theta_0] (init-DP state alpha 'theta_0)
        [state theta_1] (init-DP state alpha 'theta_1)
        phis (repeatedly K (fn [] (sample (dirichlet beta*))))
        _ (println state)
        
        ;; behavior for each word, depending on some parameters
        ;; this function definition does not depend on initial samplings!
        f (fn [phis theta] (fn [[state acc] w] (let [[state z] (S-sample state (S-discrete theta))]
                                                 (observe (discrete (nth phis z)) w)
                                                 [state (conj acc z)])))
        
        ;; applying it to each word of each document, one parameter depends on the document
        [state zs_0] (reduce (f phis theta_0) [state []] ws_0)
        _ (println state)
        [state zs_1] (reduce (f phis theta_1) [state []] ws_1)
        _ (println state)
        
        z-corpus [zs_0 zs_1]]
    (predict z-corpus)))
(def samples-step3 (doquery :lmh lda-step3 nil))
;; @@
;; ->
;;; {:theta_1 [1 1 1], :theta_0 [1 1 1]}
;;; {:theta_1 [1 1 1], :theta_0 [44 26 33]}
;;; {:theta_1 [56 15 32], :theta_0 [44 26 33]}
;;; 
;; <-
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/samples-step3</span>","value":"#'lda-transform-draft/samples-step3"}
;; <=

;; **
;;; #### Final code
;;; 
;;; Comments needed
;; **

;; @@
(defquery lda-final
  (let [[ws_0 ws_1] corpus
        
        ;; initializing
        state {}
        [state theta_0] (init-DP state alpha 'theta_0)
        [state theta_1] (init-DP state alpha 'theta_1)
        [state phis] (reduce (fn [[state acc] key] 
                               (let [[state key] (init-DP state beta* key)]
                                 [state (conj acc key)]))
                             [state []]
                             ['phi_0 'phi_1 'phi_2])
        _ (println state)
        
        ;; behavior for each word, depending on some parameters
        ;; this function definition does not depend on initial samplings!
        f (fn [phis theta] (fn [[state acc] w] (let [[state z] (S-sample state (S-discrete theta))
                                                     [state _] (S-observe state (S-discrete (nth phis z)) w)]
                                                 [state (conj acc z)])))
        
        ;; applying it to each word of each document, one parameter depends on the document
        [state zs_0] (reduce (f phis theta_0) [state []] ws_0)
        _ (println state)
        [state zs_1] (reduce (f phis theta_1) [state []] ws_1)
        _ (println state)
        
        z-corpus [zs_0 zs_1]]
    (predict z-corpus)
    (predict state)))
(def samples-final (doquery :lmh lda-final nil))
;; @@
;; ->
;;; {phi_2 [0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1], phi_1 [0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1], phi_0 [0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1], theta_1 [1 1 1], theta_0 [1 1 1]}
;;; {phi_2 [0.1 0.1 5.1 2.1 0.1 1.1 0.1 1.1 1.1 15.1], phi_1 [1.1 2.1 14.1 7.1 0.1 2.1 3.1 2.1 1.1 28.1], phi_0 [1.1 0.1 4.1 0.1 0.1 0.1 1.1 1.1 1.1 7.1], theta_1 [1 1 1], theta_0 [16 61 26]}
;;; {phi_2 [1.1 0.1 7.1 6.1 0.1 6.1 0.1 4.1 1.1 16.1], phi_1 [5.1 3.1 15.1 31.1 0.1 19.1 4.1 12.1 7.1 35.1], phi_0 [2.1 2.1 4.1 1.1 0.1 2.1 1.1 4.1 2.1 10.1], theta_1 [14 72 17], theta_0 [16 61 26]}
;;; 
;; <-
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/samples-final</span>","value":"#'lda-transform-draft/samples-final"}
;; <=

;; **
;;; ---
;; **

;; **
;;; ##Visual results
;; **

;; @@
(def samples (doquery :lmh lda-initial nil))
(def predicts (get-predicts (first samples)))
(plot-corpus corpus)
(plot-thetas (get predicts 'thetas))
(plot-phis (get predicts 'phis))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"b81e935c-b312-40d0-9fc5-3d85739f092b","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"b81e935c-b312-40d0-9fc5-3d85739f092b","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"b81e935c-b312-40d0-9fc5-3d85739f092b"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"b81e935c-b312-40d0-9fc5-3d85739f092b","values":[{"x":0,"y":0.6036638803973522},{"x":1,"y":0.008385630534177556},{"x":2,"y":6.34770524843184E-4},{"x":3,"y":0.0028616393752886548},{"x":4,"y":0.023667898132165063},{"x":5,"y":0.006995618676796648},{"x":6,"y":0.09570728254469393},{"x":7,"y":0.2580823352857086},{"x":8,"y":9.445274948809343E-7},{"x":9,"y":1.479489598679444E-12}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"b81e935c-b312-40d0-9fc5-3d85739f092b\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"b81e935c-b312-40d0-9fc5-3d85739f092b\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"b81e935c-b312-40d0-9fc5-3d85739f092b\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"b81e935c-b312-40d0-9fc5-3d85739f092b\", :values ({:x 0, :y 0.6036638803973522} {:x 1, :y 0.008385630534177556} {:x 2, :y 6.34770524843184E-4} {:x 3, :y 0.0028616393752886548} {:x 4, :y 0.023667898132165063} {:x 5, :y 0.006995618676796648} {:x 6, :y 0.09570728254469393} {:x 7, :y 0.2580823352857086} {:x 8, :y 9.445274948809343E-7} {:x 9, :y 1.479489598679444E-12})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"821a1c8d-334b-46b0-aafe-15228001dbae","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"821a1c8d-334b-46b0-aafe-15228001dbae","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"821a1c8d-334b-46b0-aafe-15228001dbae"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"821a1c8d-334b-46b0-aafe-15228001dbae","values":[{"x":0,"y":0.0019968948188105514},{"x":1,"y":0.9584816052411863},{"x":2,"y":1.6438373549309813E-10},{"x":3,"y":1.7748231378266413E-10},{"x":4,"y":4.6662599869252367E-4},{"x":5,"y":0.005925569300186667},{"x":6,"y":0.022670285964317242},{"x":7,"y":0.0010927785145064184},{"x":8,"y":0.004307717209438766},{"x":9,"y":0.005058522610995484}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"821a1c8d-334b-46b0-aafe-15228001dbae\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"821a1c8d-334b-46b0-aafe-15228001dbae\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"821a1c8d-334b-46b0-aafe-15228001dbae\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"821a1c8d-334b-46b0-aafe-15228001dbae\", :values ({:x 0, :y 0.0019968948188105514} {:x 1, :y 0.9584816052411863} {:x 2, :y 1.6438373549309813E-10} {:x 3, :y 1.7748231378266413E-10} {:x 4, :y 4.6662599869252367E-4} {:x 5, :y 0.005925569300186667} {:x 6, :y 0.022670285964317242} {:x 7, :y 0.0010927785145064184} {:x 8, :y 0.004307717209438766} {:x 9, :y 0.005058522610995484})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"bf9132dd-d7af-4dfe-8bb7-e33581cadb38","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"bf9132dd-d7af-4dfe-8bb7-e33581cadb38","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"bf9132dd-d7af-4dfe-8bb7-e33581cadb38"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"bf9132dd-d7af-4dfe-8bb7-e33581cadb38","values":[{"x":0,"y":0.051413605703235},{"x":1,"y":0.013446053052150962},{"x":2,"y":0.701170665681728},{"x":3,"y":0.15663911255943755},{"x":4,"y":3.9513494557435124E-7},{"x":5,"y":0.05689646315881443},{"x":6,"y":7.833754700898217E-5},{"x":7,"y":0.02035524258115269},{"x":8,"y":1.2458151666643376E-7},{"x":9,"y":1.0149877524721196E-14}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"bf9132dd-d7af-4dfe-8bb7-e33581cadb38\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"bf9132dd-d7af-4dfe-8bb7-e33581cadb38\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"bf9132dd-d7af-4dfe-8bb7-e33581cadb38\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"bf9132dd-d7af-4dfe-8bb7-e33581cadb38\", :values ({:x 0, :y 0.051413605703235} {:x 1, :y 0.013446053052150962} {:x 2, :y 0.701170665681728} {:x 3, :y 0.15663911255943755} {:x 4, :y 3.9513494557435124E-7} {:x 5, :y 0.05689646315881443} {:x 6, :y 7.833754700898217E-5} {:x 7, :y 0.02035524258115269} {:x 8, :y 1.2458151666643376E-7} {:x 9, :y 1.0149877524721196E-14})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}],"value":"(#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"b81e935c-b312-40d0-9fc5-3d85739f092b\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"b81e935c-b312-40d0-9fc5-3d85739f092b\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"b81e935c-b312-40d0-9fc5-3d85739f092b\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"b81e935c-b312-40d0-9fc5-3d85739f092b\", :values ({:x 0, :y 0.6036638803973522} {:x 1, :y 0.008385630534177556} {:x 2, :y 6.34770524843184E-4} {:x 3, :y 0.0028616393752886548} {:x 4, :y 0.023667898132165063} {:x 5, :y 0.006995618676796648} {:x 6, :y 0.09570728254469393} {:x 7, :y 0.2580823352857086} {:x 8, :y 9.445274948809343E-7} {:x 9, :y 1.479489598679444E-12})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"821a1c8d-334b-46b0-aafe-15228001dbae\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"821a1c8d-334b-46b0-aafe-15228001dbae\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"821a1c8d-334b-46b0-aafe-15228001dbae\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"821a1c8d-334b-46b0-aafe-15228001dbae\", :values ({:x 0, :y 0.0019968948188105514} {:x 1, :y 0.9584816052411863} {:x 2, :y 1.6438373549309813E-10} {:x 3, :y 1.7748231378266413E-10} {:x 4, :y 4.6662599869252367E-4} {:x 5, :y 0.005925569300186667} {:x 6, :y 0.022670285964317242} {:x 7, :y 0.0010927785145064184} {:x 8, :y 0.004307717209438766} {:x 9, :y 0.005058522610995484})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"bf9132dd-d7af-4dfe-8bb7-e33581cadb38\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"bf9132dd-d7af-4dfe-8bb7-e33581cadb38\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"bf9132dd-d7af-4dfe-8bb7-e33581cadb38\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"bf9132dd-d7af-4dfe-8bb7-e33581cadb38\", :values ({:x 0, :y 0.051413605703235} {:x 1, :y 0.013446053052150962} {:x 2, :y 0.701170665681728} {:x 3, :y 0.15663911255943755} {:x 4, :y 3.9513494557435124E-7} {:x 5, :y 0.05689646315881443} {:x 6, :y 7.833754700898217E-5} {:x 7, :y 0.02035524258115269} {:x 8, :y 1.2458151666643376E-7} {:x 9, :y 1.0149877524721196E-14})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}})"}
;; <=

;; @@
(defn estim-thetas [z-corpus] (map (partial proportions K) z-corpus))
(defn estim-phis [corpus z-corpus]
  (let [topic-to-words-map (apply merge-with conj (zipmap (range K) (repeat K []))
                                  (map hash-map (flatten z-corpus) (flatten corpus)))]
    (for [k (range K)] (proportions V (get topic-to-words-map k)))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/estim-phis</span>","value":"#'lda-transform-draft/estim-phis"}
;; <=

;; @@
(def samples (doquery :lmh lda-initial nil))
(def predicts (get-predicts (first samples)))
(def z-corpus (get predicts 'z-corpus))

(plot-corpus corpus)
(plot-thetas (get predicts 'thetas))
(plot-thetas (estim-thetas z-corpus))
(plot-phis (get predicts 'phis))
(plot-phis (estim-phis corpus z-corpus))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"58ed2723-8805-451e-b049-34361d8c28f7","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"58ed2723-8805-451e-b049-34361d8c28f7","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"58ed2723-8805-451e-b049-34361d8c28f7"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"58ed2723-8805-451e-b049-34361d8c28f7","values":[{"x":0,"y":0.01587301587301587},{"x":1,"y":0.03174603174603175},{"x":2,"y":0.1428571428571429},{"x":3,"y":0.1904761904761905},{"x":4,"y":0},{"x":5,"y":0.07936507936507937},{"x":6,"y":0.01587301587301587},{"x":7,"y":0.126984126984127},{"x":8,"y":0.07936507936507937},{"x":9,"y":0.3174603174603175}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"58ed2723-8805-451e-b049-34361d8c28f7\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"58ed2723-8805-451e-b049-34361d8c28f7\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"58ed2723-8805-451e-b049-34361d8c28f7\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"58ed2723-8805-451e-b049-34361d8c28f7\", :values ({:x 0, :y 1/63} {:x 1, :y 2/63} {:x 2, :y 1/7} {:x 3, :y 4/21} {:x 4, :y 0} {:x 5, :y 5/63} {:x 6, :y 1/63} {:x 7, :y 8/63} {:x 8, :y 5/63} {:x 9, :y 20/63})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"83686b5a-801c-4ca1-b552-8da0d2008e44","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"83686b5a-801c-4ca1-b552-8da0d2008e44","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"83686b5a-801c-4ca1-b552-8da0d2008e44"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"83686b5a-801c-4ca1-b552-8da0d2008e44","values":[{"x":0,"y":0.04819277108433735},{"x":1,"y":0.02409638554216867},{"x":2,"y":0.04819277108433735},{"x":3,"y":0.2771084337349398},{"x":4,"y":0},{"x":5,"y":0.2289156626506024},{"x":6,"y":0.02409638554216867},{"x":7,"y":0.1204819277108434},{"x":8,"y":0.06024096385542169},{"x":9,"y":0.1686746987951807}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"83686b5a-801c-4ca1-b552-8da0d2008e44\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"83686b5a-801c-4ca1-b552-8da0d2008e44\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"83686b5a-801c-4ca1-b552-8da0d2008e44\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"83686b5a-801c-4ca1-b552-8da0d2008e44\", :values ({:x 0, :y 4/83} {:x 1, :y 2/83} {:x 2, :y 4/83} {:x 3, :y 23/83} {:x 4, :y 0} {:x 5, :y 19/83} {:x 6, :y 2/83} {:x 7, :y 10/83} {:x 8, :y 5/83} {:x 9, :y 14/83})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"839a2bc9-49af-4f08-88de-8abaaa06de4c","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"839a2bc9-49af-4f08-88de-8abaaa06de4c","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"839a2bc9-49af-4f08-88de-8abaaa06de4c"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"839a2bc9-49af-4f08-88de-8abaaa06de4c","values":[{"x":0,"y":0.05555555555555556},{"x":1,"y":0.01851851851851852},{"x":2,"y":0.2407407407407407},{"x":3,"y":0.05555555555555556},{"x":4,"y":0},{"x":5,"y":0.05555555555555556},{"x":6,"y":0.03703703703703704},{"x":7,"y":0.03703703703703704},{"x":8,"y":0},{"x":9,"y":0.5}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"839a2bc9-49af-4f08-88de-8abaaa06de4c\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"839a2bc9-49af-4f08-88de-8abaaa06de4c\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"839a2bc9-49af-4f08-88de-8abaaa06de4c\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"839a2bc9-49af-4f08-88de-8abaaa06de4c\", :values ({:x 0, :y 1/18} {:x 1, :y 1/54} {:x 2, :y 13/54} {:x 3, :y 1/18} {:x 4, :y 0} {:x 5, :y 1/18} {:x 6, :y 1/27} {:x 7, :y 1/27} {:x 8, :y 0} {:x 9, :y 1/2})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}],"value":"(#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"58ed2723-8805-451e-b049-34361d8c28f7\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"58ed2723-8805-451e-b049-34361d8c28f7\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"58ed2723-8805-451e-b049-34361d8c28f7\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"58ed2723-8805-451e-b049-34361d8c28f7\", :values ({:x 0, :y 1/63} {:x 1, :y 2/63} {:x 2, :y 1/7} {:x 3, :y 4/21} {:x 4, :y 0} {:x 5, :y 5/63} {:x 6, :y 1/63} {:x 7, :y 8/63} {:x 8, :y 5/63} {:x 9, :y 20/63})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"83686b5a-801c-4ca1-b552-8da0d2008e44\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"83686b5a-801c-4ca1-b552-8da0d2008e44\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"83686b5a-801c-4ca1-b552-8da0d2008e44\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"83686b5a-801c-4ca1-b552-8da0d2008e44\", :values ({:x 0, :y 4/83} {:x 1, :y 2/83} {:x 2, :y 4/83} {:x 3, :y 23/83} {:x 4, :y 0} {:x 5, :y 19/83} {:x 6, :y 2/83} {:x 7, :y 10/83} {:x 8, :y 5/83} {:x 9, :y 14/83})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"839a2bc9-49af-4f08-88de-8abaaa06de4c\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"839a2bc9-49af-4f08-88de-8abaaa06de4c\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"839a2bc9-49af-4f08-88de-8abaaa06de4c\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"839a2bc9-49af-4f08-88de-8abaaa06de4c\", :values ({:x 0, :y 1/18} {:x 1, :y 1/54} {:x 2, :y 13/54} {:x 3, :y 1/18} {:x 4, :y 0} {:x 5, :y 1/18} {:x 6, :y 1/27} {:x 7, :y 1/27} {:x 8, :y 0} {:x 9, :y 1/2})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}})"}
;; <=

;; @@
(def samples (doquery :lmh lda-final nil))
(def predicts (get-predicts (first samples)))
(def state (get predicts 'state))

(plot-corpus corpus)
(plot-thetas (map #(get state %) ['theta_0 'theta_1]))
(plot-phis (map #(get state %) ['phi_0 'phi_1 'phi_2]))
;; @@
;; ->
;;; {phi_2 [0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1], phi_1 [0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1], phi_0 [0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1], theta_1 [1 1 1], theta_0 [1 1 1]}
;;; {phi_2 [0.1 0.1 8.1 5.1 0.1 0.1 0.1 1.1 1.1 14.1], phi_1 [0.1 0.1 1.1 0.1 0.1 0.1 0.1 1.1 0.1 2.1], phi_0 [2.1 2.1 14.1 4.1 0.1 3.1 4.1 2.1 2.1 34.1], theta_1 [1 1 1], theta_0 [68 5 30]}
;;; {phi_2 [0.1 0.1 8.1 7.1 0.1 0.1 0.1 2.1 2.1 14.1], phi_1 [6.1 3.1 4.1 25.1 0.1 20.1 0.1 13.1 6.1 11.1], phi_0 [2.1 2.1 14.1 6.1 0.1 7.1 5.1 5.1 2.1 36.1], theta_1 [13 85 5], theta_0 [68 5 30]}
;;; {phi_2 [0.1 0.1 8.1 5.1 0.1 0.1 0.1 1.1 1.1 14.1], phi_1 [0.1 0.1 1.1 0.1 0.1 0.1 0.1 1.1 0.1 2.1], phi_0 [2.1 2.1 14.1 4.1 0.1 3.1 4.1 2.1 2.1 34.1], theta_1 [1 1 1], theta_0 [68 5 30]}
;;; {phi_2 [0.1 0.1 8.1 7.1 0.1 0.1 0.1 2.1 2.1 14.1], phi_1 [6.1 3.1 4.1 25.1 0.1 20.1 0.1 13.1 6.1 11.1], phi_0 [2.1 2.1 14.1 6.1 0.1 7.1 5.1 5.1 2.1 36.1], theta_1 [13 85 5], theta_0 [68 5 30]}
;;; 
;; <-
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52","values":[{"x":0,"y":2.1},{"x":1,"y":2.1},{"x":2,"y":14.1},{"x":3,"y":6.1},{"x":4,"y":0.1},{"x":5,"y":7.1},{"x":6,"y":5.1},{"x":7,"y":5.1},{"x":8,"y":2.1},{"x":9,"y":36.1}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52\", :values ({:x 0, :y 2.1} {:x 1, :y 2.1} {:x 2, :y 14.1} {:x 3, :y 6.1} {:x 4, :y 0.1} {:x 5, :y 7.1} {:x 6, :y 5.1} {:x 7, :y 5.1} {:x 8, :y 2.1} {:x 9, :y 36.1})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"8651485e-b95e-45db-bcf0-a4a39b8e14c9","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"8651485e-b95e-45db-bcf0-a4a39b8e14c9","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"8651485e-b95e-45db-bcf0-a4a39b8e14c9"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"8651485e-b95e-45db-bcf0-a4a39b8e14c9","values":[{"x":0,"y":6.1},{"x":1,"y":3.1},{"x":2,"y":4.1},{"x":3,"y":25.1},{"x":4,"y":0.1},{"x":5,"y":20.1},{"x":6,"y":0.1},{"x":7,"y":13.1},{"x":8,"y":6.1},{"x":9,"y":11.1}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"8651485e-b95e-45db-bcf0-a4a39b8e14c9\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"8651485e-b95e-45db-bcf0-a4a39b8e14c9\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"8651485e-b95e-45db-bcf0-a4a39b8e14c9\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"8651485e-b95e-45db-bcf0-a4a39b8e14c9\", :values ({:x 0, :y 6.1} {:x 1, :y 3.1} {:x 2, :y 4.1} {:x 3, :y 25.1} {:x 4, :y 0.1} {:x 5, :y 20.1} {:x 6, :y 0.1} {:x 7, :y 13.1} {:x 8, :y 6.1} {:x 9, :y 11.1})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"c1da7edd-f4fe-4bff-9927-d2446a6d82ee","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"c1da7edd-f4fe-4bff-9927-d2446a6d82ee","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"c1da7edd-f4fe-4bff-9927-d2446a6d82ee"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"c1da7edd-f4fe-4bff-9927-d2446a6d82ee","values":[{"x":0,"y":0.1},{"x":1,"y":0.1},{"x":2,"y":8.1},{"x":3,"y":7.1},{"x":4,"y":0.1},{"x":5,"y":0.1},{"x":6,"y":0.1},{"x":7,"y":2.1},{"x":8,"y":2.1},{"x":9,"y":14.1}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"c1da7edd-f4fe-4bff-9927-d2446a6d82ee\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"c1da7edd-f4fe-4bff-9927-d2446a6d82ee\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"c1da7edd-f4fe-4bff-9927-d2446a6d82ee\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"c1da7edd-f4fe-4bff-9927-d2446a6d82ee\", :values ({:x 0, :y 0.1} {:x 1, :y 0.1} {:x 2, :y 8.1} {:x 3, :y 7.1} {:x 4, :y 0.1} {:x 5, :y 0.1} {:x 6, :y 0.1} {:x 7, :y 2.1} {:x 8, :y 2.1} {:x 9, :y 14.1})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}],"value":"(#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"fcb6fa4f-5fc3-4c2e-b932-0cdd53612f52\", :values ({:x 0, :y 2.1} {:x 1, :y 2.1} {:x 2, :y 14.1} {:x 3, :y 6.1} {:x 4, :y 0.1} {:x 5, :y 7.1} {:x 6, :y 5.1} {:x 7, :y 5.1} {:x 8, :y 2.1} {:x 9, :y 36.1})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"8651485e-b95e-45db-bcf0-a4a39b8e14c9\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"8651485e-b95e-45db-bcf0-a4a39b8e14c9\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"8651485e-b95e-45db-bcf0-a4a39b8e14c9\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"8651485e-b95e-45db-bcf0-a4a39b8e14c9\", :values ({:x 0, :y 6.1} {:x 1, :y 3.1} {:x 2, :y 4.1} {:x 3, :y 25.1} {:x 4, :y 0.1} {:x 5, :y 20.1} {:x 6, :y 0.1} {:x 7, :y 13.1} {:x 8, :y 6.1} {:x 9, :y 11.1})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"c1da7edd-f4fe-4bff-9927-d2446a6d82ee\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"c1da7edd-f4fe-4bff-9927-d2446a6d82ee\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"c1da7edd-f4fe-4bff-9927-d2446a6d82ee\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"c1da7edd-f4fe-4bff-9927-d2446a6d82ee\", :values ({:x 0, :y 0.1} {:x 1, :y 0.1} {:x 2, :y 8.1} {:x 3, :y 7.1} {:x 4, :y 0.1} {:x 5, :y 0.1} {:x 6, :y 0.1} {:x 7, :y 2.1} {:x 8, :y 2.1} {:x 9, :y 14.1})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}})"}
;; <=

;; @@
;; an example of the state we get when running lda-final
;; topics 0 and 1 look really like documents 0 and 1 respectively

(def state {'phi_2 [0.1 0.1 8.1 7.1 0.1 0.1 0.1 2.1 2.1 14.1], 'phi_1 [6.1 3.1 4.1 25.1 0.1 20.1 0.1 13.1 6.1 11.1], 'phi_0 [2.1 2.1 14.1 6.1 0.1 7.1 5.1 5.1 2.1 36.1], 'theta_1 [13 85 5], 'theta_0 [68 5 30]})

(plot-corpus corpus)
(plot-thetas (map #(get state %) ['theta_0 'theta_1]))
(plot-phis (map #(get state %) ['phi_0 'phi_1 'phi_2]))
(defn f [a] (sample (dirichlet (get state a))))
(plot-thetas (map f ['theta_0 'theta_1]))
(plot-phis (map f ['phi_0 'phi_1 'phi_2]))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"0338179e-497b-47cf-ab4e-899d569026ef","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"0338179e-497b-47cf-ab4e-899d569026ef","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"0338179e-497b-47cf-ab4e-899d569026ef"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"0338179e-497b-47cf-ab4e-899d569026ef","values":[{"x":0,"y":0.037106521836193354},{"x":1,"y":0.013993392626933332},{"x":2,"y":0.2555845898170262},{"x":3,"y":0.09794126122890591},{"x":4,"y":2.0020536034851604E-10},{"x":5,"y":0.13518378032917686},{"x":6,"y":0.040919033234662915},{"x":7,"y":0.024102668743249497},{"x":8,"y":0.02628180688396762},{"x":9,"y":0.3688869450996789}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"0338179e-497b-47cf-ab4e-899d569026ef\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"0338179e-497b-47cf-ab4e-899d569026ef\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"0338179e-497b-47cf-ab4e-899d569026ef\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"0338179e-497b-47cf-ab4e-899d569026ef\", :values ({:x 0, :y 0.037106521836193354} {:x 1, :y 0.013993392626933332} {:x 2, :y 0.2555845898170262} {:x 3, :y 0.09794126122890591} {:x 4, :y 2.0020536034851604E-10} {:x 5, :y 0.13518378032917686} {:x 6, :y 0.040919033234662915} {:x 7, :y 0.024102668743249497} {:x 8, :y 0.02628180688396762} {:x 9, :y 0.3688869450996789})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"bfc8adac-911a-4994-a2e1-87909b47988a","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"bfc8adac-911a-4994-a2e1-87909b47988a","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"bfc8adac-911a-4994-a2e1-87909b47988a"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"bfc8adac-911a-4994-a2e1-87909b47988a","values":[{"x":0,"y":0.05315260543423704},{"x":1,"y":0.02550505283134817},{"x":2,"y":0.07947947595208445},{"x":3,"y":0.20682936180043465},{"x":4,"y":5.881578595626933E-10},{"x":5,"y":0.1648627218968749},{"x":6,"y":0.002081499645178328},{"x":7,"y":0.15735024540321196},{"x":8,"y":0.08011098434301321},{"x":9,"y":0.23062805210545947}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"bfc8adac-911a-4994-a2e1-87909b47988a\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"bfc8adac-911a-4994-a2e1-87909b47988a\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"bfc8adac-911a-4994-a2e1-87909b47988a\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"bfc8adac-911a-4994-a2e1-87909b47988a\", :values ({:x 0, :y 0.05315260543423704} {:x 1, :y 0.02550505283134817} {:x 2, :y 0.07947947595208445} {:x 3, :y 0.20682936180043465} {:x 4, :y 5.881578595626933E-10} {:x 5, :y 0.1648627218968749} {:x 6, :y 0.002081499645178328} {:x 7, :y 0.15735024540321196} {:x 8, :y 0.08011098434301321} {:x 9, :y 0.23062805210545947})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"01819044-c772-4158-9865-83c7f3b526ff","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"01819044-c772-4158-9865-83c7f3b526ff","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"01819044-c772-4158-9865-83c7f3b526ff"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"01819044-c772-4158-9865-83c7f3b526ff","values":[{"x":0,"y":0.011947815526822536},{"x":1,"y":1.0918816068978355E-11},{"x":2,"y":0.16983944570991996},{"x":3,"y":0.25461970321406263},{"x":4,"y":0.014886966806378618},{"x":5,"y":1.2321879188210202E-4},{"x":6,"y":3.7111821974482935E-5},{"x":7,"y":0.1098978452675571},{"x":8,"y":0.020164085453865093},{"x":9,"y":0.4184838073966186}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"01819044-c772-4158-9865-83c7f3b526ff\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"01819044-c772-4158-9865-83c7f3b526ff\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"01819044-c772-4158-9865-83c7f3b526ff\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"01819044-c772-4158-9865-83c7f3b526ff\", :values ({:x 0, :y 0.011947815526822536} {:x 1, :y 1.0918816068978355E-11} {:x 2, :y 0.16983944570991996} {:x 3, :y 0.25461970321406263} {:x 4, :y 0.014886966806378618} {:x 5, :y 1.2321879188210202E-4} {:x 6, :y 3.7111821974482935E-5} {:x 7, :y 0.1098978452675571} {:x 8, :y 0.020164085453865093} {:x 9, :y 0.4184838073966186})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}],"value":"(#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"0338179e-497b-47cf-ab4e-899d569026ef\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"0338179e-497b-47cf-ab4e-899d569026ef\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"0338179e-497b-47cf-ab4e-899d569026ef\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"0338179e-497b-47cf-ab4e-899d569026ef\", :values ({:x 0, :y 0.037106521836193354} {:x 1, :y 0.013993392626933332} {:x 2, :y 0.2555845898170262} {:x 3, :y 0.09794126122890591} {:x 4, :y 2.0020536034851604E-10} {:x 5, :y 0.13518378032917686} {:x 6, :y 0.040919033234662915} {:x 7, :y 0.024102668743249497} {:x 8, :y 0.02628180688396762} {:x 9, :y 0.3688869450996789})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"bfc8adac-911a-4994-a2e1-87909b47988a\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"bfc8adac-911a-4994-a2e1-87909b47988a\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"bfc8adac-911a-4994-a2e1-87909b47988a\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"bfc8adac-911a-4994-a2e1-87909b47988a\", :values ({:x 0, :y 0.05315260543423704} {:x 1, :y 0.02550505283134817} {:x 2, :y 0.07947947595208445} {:x 3, :y 0.20682936180043465} {:x 4, :y 5.881578595626933E-10} {:x 5, :y 0.1648627218968749} {:x 6, :y 0.002081499645178328} {:x 7, :y 0.15735024540321196} {:x 8, :y 0.08011098434301321} {:x 9, :y 0.23062805210545947})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"01819044-c772-4158-9865-83c7f3b526ff\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"01819044-c772-4158-9865-83c7f3b526ff\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"01819044-c772-4158-9865-83c7f3b526ff\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"01819044-c772-4158-9865-83c7f3b526ff\", :values ({:x 0, :y 0.011947815526822536} {:x 1, :y 1.0918816068978355E-11} {:x 2, :y 0.16983944570991996} {:x 3, :y 0.25461970321406263} {:x 4, :y 0.014886966806378618} {:x 5, :y 1.2321879188210202E-4} {:x 6, :y 3.7111821974482935E-5} {:x 7, :y 0.1098978452675571} {:x 8, :y 0.020164085453865093} {:x 9, :y 0.4184838073966186})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}})"}
;; <=

;; @@
(def samples (doquery :lmh lda-final nil))
(def predicts (get-predicts (first samples)))
(def z-corpus (get predicts 'z-corpus))

(plot-corpus corpus)
(plot-thetas (estim-thetas z-corpus))
(plot-phis (estim-phis corpus z-corpus))
;; @@
;; ->
;;; {phi_2 [0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1], phi_1 [0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1], phi_0 [0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1 0.1], theta_1 [1 1 1], theta_0 [1 1 1]}
;;; {phi_2 [2.1 1.1 13.1 3.1 0.1 0.1 2.1 3.1 1.1 26.1], phi_1 [0.1 0.1 2.1 3.1 0.1 0.1 0.1 0.1 1.1 8.1], phi_0 [0.1 1.1 8.1 3.1 0.1 3.1 2.1 1.1 1.1 16.1], theta_1 [1 1 1], theta_0 [36 15 52]}
;;; {phi_2 [3.1 2.1 13.1 10.1 0.1 5.1 2.1 8.1 2.1 28.1], phi_1 [5.1 2.1 5.1 25.1 0.1 19.1 1.1 11.1 7.1 17.1], phi_0 [0.1 1.1 8.1 3.1 0.1 3.1 2.1 1.1 1.1 16.1], theta_1 [1 79 23], theta_0 [36 15 52]}
;;; {phi_2 [2.1 1.1 13.1 3.1 0.1 0.1 2.1 3.1 1.1 27.1], phi_1 [0.1 0.1 2.1 3.1 0.1 0.1 0.1 0.1 1.1 8.1], phi_0 [0.1 1.1 8.1 3.1 0.1 3.1 2.1 1.1 1.1 15.1], theta_1 [1 1 1], theta_0 [35 15 53]}
;;; {phi_2 [3.1 2.1 13.1 10.1 0.1 5.1 2.1 8.1 2.1 29.1], phi_1 [5.1 2.1 5.1 25.1 0.1 19.1 1.1 11.1 7.1 17.1], phi_0 [0.1 1.1 8.1 3.1 0.1 3.1 2.1 1.1 1.1 15.1], theta_1 [1 79 23], theta_0 [35 15 53]}
;;; 
;; <-
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"492aff27-c9eb-41dd-9094-d61dda371a8e","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"492aff27-c9eb-41dd-9094-d61dda371a8e","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"492aff27-c9eb-41dd-9094-d61dda371a8e"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"492aff27-c9eb-41dd-9094-d61dda371a8e","values":[{"x":0,"y":0},{"x":1,"y":0.02941176470588235},{"x":2,"y":0.2352941176470588},{"x":3,"y":0.08823529411764706},{"x":4,"y":0},{"x":5,"y":0.08823529411764706},{"x":6,"y":0.05882352941176471},{"x":7,"y":0.02941176470588235},{"x":8,"y":0.02941176470588235},{"x":9,"y":0.4411764705882353}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"492aff27-c9eb-41dd-9094-d61dda371a8e\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"492aff27-c9eb-41dd-9094-d61dda371a8e\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"492aff27-c9eb-41dd-9094-d61dda371a8e\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"492aff27-c9eb-41dd-9094-d61dda371a8e\", :values ({:x 0, :y 0} {:x 1, :y 1/34} {:x 2, :y 4/17} {:x 3, :y 3/34} {:x 4, :y 0} {:x 5, :y 3/34} {:x 6, :y 1/17} {:x 7, :y 1/34} {:x 8, :y 1/34} {:x 9, :y 15/34})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"7ef1c699-759b-4e7d-967a-19c2d4d7901a","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"7ef1c699-759b-4e7d-967a-19c2d4d7901a","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"7ef1c699-759b-4e7d-967a-19c2d4d7901a"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"7ef1c699-759b-4e7d-967a-19c2d4d7901a","values":[{"x":0,"y":0.05434782608695652},{"x":1,"y":0.02173913043478261},{"x":2,"y":0.05434782608695652},{"x":3,"y":0.2717391304347826},{"x":4,"y":0},{"x":5,"y":0.2065217391304348},{"x":6,"y":0.0108695652173913},{"x":7,"y":0.1195652173913043},{"x":8,"y":0.07608695652173914},{"x":9,"y":0.1847826086956522}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"7ef1c699-759b-4e7d-967a-19c2d4d7901a\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"7ef1c699-759b-4e7d-967a-19c2d4d7901a\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"7ef1c699-759b-4e7d-967a-19c2d4d7901a\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"7ef1c699-759b-4e7d-967a-19c2d4d7901a\", :values ({:x 0, :y 5/92} {:x 1, :y 1/46} {:x 2, :y 5/92} {:x 3, :y 25/92} {:x 4, :y 0} {:x 5, :y 19/92} {:x 6, :y 1/92} {:x 7, :y 11/92} {:x 8, :y 7/92} {:x 9, :y 17/92})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"},{"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"ordinal","range":"width","domain":{"data":"ca0f0a8b-d279-4c63-9110-167e88782831","field":"data.x"}},{"name":"y","range":"height","nice":true,"domain":{"data":"ca0f0a8b-d279-4c63-9110-167e88782831","field":"data.y"}}],"marks":[{"type":"rect","from":{"data":"ca0f0a8b-d279-4c63-9110-167e88782831"},"properties":{"enter":{"y":{"scale":"y","field":"data.y"},"width":{"offset":-1,"scale":"x","band":true},"x":{"scale":"x","field":"data.x"},"y2":{"scale":"y","value":0}},"update":{"fill":{"value":"steelblue"},"opacity":{"value":1}},"hover":{"fill":{"value":"#FF29D2"}}}}],"data":[{"name":"ca0f0a8b-d279-4c63-9110-167e88782831","values":[{"x":0,"y":0.04054054054054054},{"x":1,"y":0.02702702702702703},{"x":2,"y":0.1756756756756757},{"x":3,"y":0.1351351351351351},{"x":4,"y":0},{"x":5,"y":0.06756756756756757},{"x":6,"y":0.02702702702702703},{"x":7,"y":0.1081081081081081},{"x":8,"y":0.02702702702702703},{"x":9,"y":0.3918918918918919}]}],"width":100,"height":61.8046989440918,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"ca0f0a8b-d279-4c63-9110-167e88782831\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"ca0f0a8b-d279-4c63-9110-167e88782831\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"ca0f0a8b-d279-4c63-9110-167e88782831\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"ca0f0a8b-d279-4c63-9110-167e88782831\", :values ({:x 0, :y 3/74} {:x 1, :y 1/37} {:x 2, :y 13/74} {:x 3, :y 5/37} {:x 4, :y 0} {:x 5, :y 5/74} {:x 6, :y 1/37} {:x 7, :y 4/37} {:x 8, :y 1/37} {:x 9, :y 29/74})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}],"value":"(#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"492aff27-c9eb-41dd-9094-d61dda371a8e\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"492aff27-c9eb-41dd-9094-d61dda371a8e\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"492aff27-c9eb-41dd-9094-d61dda371a8e\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"492aff27-c9eb-41dd-9094-d61dda371a8e\", :values ({:x 0, :y 0} {:x 1, :y 1/34} {:x 2, :y 4/17} {:x 3, :y 3/34} {:x 4, :y 0} {:x 5, :y 3/34} {:x 6, :y 1/17} {:x 7, :y 1/34} {:x 8, :y 1/34} {:x 9, :y 15/34})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"7ef1c699-759b-4e7d-967a-19c2d4d7901a\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"7ef1c699-759b-4e7d-967a-19c2d4d7901a\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"7ef1c699-759b-4e7d-967a-19c2d4d7901a\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"7ef1c699-759b-4e7d-967a-19c2d4d7901a\", :values ({:x 0, :y 5/92} {:x 1, :y 1/46} {:x 2, :y 5/92} {:x 3, :y 25/92} {:x 4, :y 0} {:x 5, :y 19/92} {:x 6, :y 1/92} {:x 7, :y 11/92} {:x 8, :y 7/92} {:x 9, :y 17/92})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}} #gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"ordinal\", :range \"width\", :domain {:data \"ca0f0a8b-d279-4c63-9110-167e88782831\", :field \"data.x\"}} {:name \"y\", :range \"height\", :nice true, :domain {:data \"ca0f0a8b-d279-4c63-9110-167e88782831\", :field \"data.y\"}}], :marks [{:type \"rect\", :from {:data \"ca0f0a8b-d279-4c63-9110-167e88782831\"}, :properties {:enter {:y {:scale \"y\", :field \"data.y\"}, :width {:offset -1, :scale \"x\", :band true}, :x {:scale \"x\", :field \"data.x\"}, :y2 {:scale \"y\", :value 0}}, :update {:fill {:value \"steelblue\"}, :opacity {:value 1}}, :hover {:fill {:value \"#FF29D2\"}}}}], :data [{:name \"ca0f0a8b-d279-4c63-9110-167e88782831\", :values ({:x 0, :y 3/74} {:x 1, :y 1/37} {:x 2, :y 13/74} {:x 3, :y 5/37} {:x 4, :y 0} {:x 5, :y 5/74} {:x 6, :y 1/37} {:x 7, :y 4/37} {:x 8, :y 1/37} {:x 9, :y 29/74})}], :width 100, :height 61.8047, :padding {:bottom 20, :top 10, :right 10, :left 50}}})"}
;; <=

;; **
;;; ---
;; **

;; **
;;; ---
;; **

;; **
;;; #Abandoned ideas
;; **

;; **
;;; Step 1: Marginalizing over theta_0
;;; --
;;; 
;;; Conditions allowing it:
;;; - (1) it is sampled from a Dirichlet distribution
;;; - (2) it only appears as an argument for a discrete distribution. Note that it is used in a call to f, but this is fine since the argument theta of f only appears as (discrete theta) in the body of f.
;;; - (3) the Dirichlet distribution is a conjugate prior with a discrete likelihood.
;;; 
;;; Program transformation:
;;; - (a) Replace all occurrences of (discrete theta_0) by a distribution variable d-theta_0. Regarding f: replace f with a function f1 which takes a distribution d-theta as an argument instead of theta, and in all calls where theta is not theta_0, replace it with (discrete theta).
;;; - (b) Change the definition of d-theta_0.......
;; **

;; @@
(defquery step1a
  (let [[ws_0 ws_1] corpus
        
        ;; initial samplings
        d-theta_0 (discrete (sample (dirichlet alpha)))
        theta_1 (sample (dirichlet alpha))
        phis (repeatedly K (fn [] (sample (dirichlet beta*))))
        
        ;; behavior for each word, depending on some parameters
        ;; this function definition does not depend on initial samplings!
        f1 (fn [phis d-theta] (fn [w] (let [z (sample d-theta)]
                                     (observe (discrete (nth phis z)) w)
                                     z)))
        
        ;; applying it to each word of each document, one parameter depends on the document
        zs_0 (map (f1 phis d-theta_0) ws_0)
        zs_1 (map (f1 phis (discrete theta_1)) ws_1)
        
        z-corpus [zs_0 zs_1]]
    (predict z-corpus)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/step1a</span>","value":"#'lda-transform-draft/step1a"}
;; <=

;; @@
(defdist test-1arg [x] [dist (uniform-continuous 0 1)]
  (sample [this] (+ x (sample dist)))
  (observe [this value] (observe dist (- value x))))
(def x (sample (flip 0.5)))
x
(defm f [x] (store :x x))
(with-primitive-procedures [f] (f x))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;lda-transform-draft/f</span>","value":"#'lda-transform-draft/f"}
;; <=

;; @@
(defdist dirichlet-process
  "Dirichlet process (side effects in sample and observe)"
  [alpha]
  [key (gensym "dp")
   _ (store key (vec alpha))]
  (sample [this] (let [alpha* (retrieve key)
                       value (sample (discrete alpha*))]
                   (store key (update-in alpha* [value] inc))
                   value))
  (observe [this value] (let [alpha* (retrieve key)]
                          (store key (update-in alpha* [value] inc))
                          (observe (discrete alpha*) value))))
;; @@

;; gorilla-repl.fileformat = 1

;; **
;;; Boiler-plate code --- importing necessary things.
;; **

;; @@
(use 'nstools.ns)
(ns+ dirichlet-prior
  (:like anglican-user.worksheet))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; Let us learn a categorical distribution with a dirichlet prior, using two different methods.
;; **

;; **
;;; We generate such a distribution and training data.
;; **

;; @@
(def N 10)
(def alpha (repeat N 1))
(def p (sample (dirichlet alpha)))
(def dist (discrete p))
(def training (repeatedly #(sample dist)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/training</span>","value":"#'dirichlet/training"}
;; <=

;; @@
p
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-double'>0.11048610910276664</span>","value":"0.11048610910276664"},{"type":"html","content":"<span class='clj-double'>0.07947184392024659</span>","value":"0.07947184392024659"},{"type":"html","content":"<span class='clj-double'>0.08451143669759656</span>","value":"0.08451143669759656"},{"type":"html","content":"<span class='clj-double'>0.18309238049501483</span>","value":"0.18309238049501483"},{"type":"html","content":"<span class='clj-double'>0.13180028009927752</span>","value":"0.13180028009927752"},{"type":"html","content":"<span class='clj-double'>0.0326563235591526</span>","value":"0.0326563235591526"},{"type":"html","content":"<span class='clj-double'>0.06247129994737716</span>","value":"0.06247129994737716"},{"type":"html","content":"<span class='clj-double'>0.010424371511980397</span>","value":"0.010424371511980397"},{"type":"html","content":"<span class='clj-double'>0.23041673704601848</span>","value":"0.23041673704601848"},{"type":"html","content":"<span class='clj-double'>0.07466921762056934</span>","value":"0.07466921762056934"}],"value":"(0.11048610910276664 0.07947184392024659 0.08451143669759656 0.18309238049501483 0.13180028009927752 0.0326563235591526 0.06247129994737716 0.010424371511980397 0.23041673704601848 0.07466921762056934)"}
;; <=

;; @@
(take 50 training)
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-long'>8</span>","value":"8"},{"type":"html","content":"<span class='clj-long'>5</span>","value":"5"},{"type":"html","content":"<span class='clj-long'>0</span>","value":"0"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>0</span>","value":"0"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>0</span>","value":"0"},{"type":"html","content":"<span class='clj-long'>8</span>","value":"8"},{"type":"html","content":"<span class='clj-long'>8</span>","value":"8"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"},{"type":"html","content":"<span class='clj-long'>8</span>","value":"8"},{"type":"html","content":"<span class='clj-long'>6</span>","value":"6"},{"type":"html","content":"<span class='clj-long'>6</span>","value":"6"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>8</span>","value":"8"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>8</span>","value":"8"},{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"html","content":"<span class='clj-long'>6</span>","value":"6"},{"type":"html","content":"<span class='clj-long'>6</span>","value":"6"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"},{"type":"html","content":"<span class='clj-long'>5</span>","value":"5"},{"type":"html","content":"<span class='clj-long'>6</span>","value":"6"},{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"},{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>0</span>","value":"0"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"},{"type":"html","content":"<span class='clj-long'>0</span>","value":"0"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>8</span>","value":"8"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"html","content":"<span class='clj-long'>8</span>","value":"8"}],"value":"(8 5 0 9 0 2 1 9 0 8 8 2 4 9 2 3 8 6 6 9 8 1 8 4 9 2 6 6 3 5 6 4 1 9 2 9 3 4 3 1 0 9 4 9 9 0 1 8 2 8)"}
;; <=

;; **
;;; First method: a lot of observes.
;; **

;; @@
(defquery dirichlet-prior-observes
  (let [p (sample (dirichlet alpha))
        d (discrete p)]
    (reduce (fn [_ x] (observe d x)) nil (take 10000 training))
    (predict 'x (sample d))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/dirichlet-prior-observes</span>","value":"#'dirichlet/dirichlet-prior-observes"}
;; <=

;; @@
(def samples-observes (doquery :lmh dirichlet-prior-observes nil))
(def xs-observes (map #(get % 'x) (map get-predicts samples-observes)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/xs-observes</span>","value":"#'dirichlet/xs-observes"}
;; <=

;; **
;;; Second method: dynamic process.
;; **

;; @@
(defproc DSD*
  "discrete-symmetric-dirichlet process"
  [alpha] [counts (vec alpha)]
  (produce [this] (discrete counts))
  (absorb [this sample]
    (DSD* alpha (update-in counts [sample] + 1.))))
(with-primitive-procedures [DSD*]
  (defm DSD 
    "discrete-symmetric-dirichlet process"
    [& args] (apply DSD* args)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/DSD</span>","value":"#'dirichlet/DSD"}
;; <=

;; @@
(defquery dirichlet-prior-process
  (loop [proc (DSD alpha)
         data (take 1000 training)]
    (if (seq data)
      (let [[x & data-] data]
        (recur (absorb proc x) data-))
      (predict 'x (sample (produce proc))))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/dirichlet-prior-process</span>","value":"#'dirichlet/dirichlet-prior-process"}
;; <=

;; @@
(def samples-process (doquery :lmh dirichlet-prior-process nil))
(def xs-process (map #(get % 'x) (map get-predicts samples-process)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;dirichlet/xs-process</span>","value":"#'dirichlet/xs-process"}
;; <=

;; **
;;; Results: we compute the frequencies of each point over a 1000 samples from the original distribution or from the ones we have learnt. Unsurprisingly, the second method seems better than the first one, although the second method uses only 1000 training points (stack overflow with 10000) while the first method uses 10000.
;; **

;; @@
(defn frequencies-list [xs]
  (map #(get (frequencies xs) %) (range 10)))
(frequencies-list (repeatedly 1000 #(sample (discrete p))))
(frequencies-list (take 1000 xs-observes))
(frequencies-list (take 1000 xs-process))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-lazy-seq'>(</span>","close":"<span class='clj-lazy-seq'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-long'>132</span>","value":"132"},{"type":"html","content":"<span class='clj-long'>86</span>","value":"86"},{"type":"html","content":"<span class='clj-long'>75</span>","value":"75"},{"type":"html","content":"<span class='clj-long'>172</span>","value":"172"},{"type":"html","content":"<span class='clj-long'>149</span>","value":"149"},{"type":"html","content":"<span class='clj-long'>42</span>","value":"42"},{"type":"html","content":"<span class='clj-long'>49</span>","value":"49"},{"type":"html","content":"<span class='clj-long'>15</span>","value":"15"},{"type":"html","content":"<span class='clj-long'>201</span>","value":"201"},{"type":"html","content":"<span class='clj-long'>79</span>","value":"79"}],"value":"(132 86 75 172 149 42 49 15 201 79)"}
;; <=

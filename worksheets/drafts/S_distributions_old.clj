;; gorilla-repl.fileformat = 1

;; **
;;; Boiler-plate code --- importing necessary things.
;; **

;; @@
(use 'nstools.ns)
(ns+ S-distributions-old
  (:like anglican-user.worksheet))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; S-distributions are related to sample and observe functions which behave like those of a usual distribution, exept that they also take and return a state.
;;; 
;;; An S-distribution may be defined from the same arguments as the corresponding usual distribution, in which case the functions leave the state unchanged and otherwise have the usual behavior. Or it may be defined from arguments of a different type, in which case they are the keys to information stored in the state which is relevant to a dynamic process.
;;; 
;;; S-distributions are implemented as maps {:sample <function> :observe <function>}. Ideally I should write a macro similar to defdist, but I don't know how to do it and this is enough to experiment right now.
;; **

;; @@
(defm S-sample [state S-dist] ((get S-dist :sample) state))
(defm S-observe [state S-dist value] ((get S-dist :observe) state value))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;S-distributions-old/S-observe</span>","value":"#'S-distributions-old/S-observe"}
;; <=

;; **
;;; #### Dirichlet process: Discrete distribution with a Dirichlet prior
;; **

;; @@
(defm inc-nth [v n] (assoc v n (inc (get v n))))
(defm S-discrete-utils
  "Used in S-discrete. Gets the underlying usual distribution
  and a function to update the state."
  [state p]
  (if (sequential? p)
    [(discrete p)
     (fn [value] state)]
    (let [p* (get state p)]
      [(discrete p*)
       (fn [value] (assoc state p (inc-nth p* value)))])))

(defm S-discrete
  "Creates an S-distribution corresponding to a discrete distribution."
  [p]
  {:sample (fn [state] (let [[dist update-state] (S-discrete-utils state p)
                             value (sample dist)]
                         [(update-state value) value]))
   :observe (fn [state value] (let [[dist update-state] (S-discrete-utils state p)]
                                [(update-state value) (observe dist value)]))})

(defn init-DP*
  ([state alpha] (init-DP* state alpha (gensym "DPvec")))
  ([state alpha key] [(assoc state key alpha) key]))
(with-primitive-procedures [init-DP*]
  (defm init-DP [& args] (apply init-DP* args)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;S-distributions-old/init-DP</span>","value":"#'S-distributions-old/init-DP"}
;; <=
